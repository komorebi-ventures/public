# The Good:
These are books that I have read and reread over the last few years. They are full of exceptional information and can help anyone who reads them gain a ton of great ideas to try out in real life.

## Permission to Screw Up: How I Learned to Lead by Doing (Almost) Everything Wrong
[Audible](https://a.co/d/aZyNG2A)

## The Infinite Game
### Teaser: Learn a different way to think of business that will help your company keep playing when your rivals simply can’t.

[Audible](https://www.amazon.com/The-Infinite-Game-Simon-Sinek-audiobook/dp/B07DKHFTB7/ref=tmm_aud_swatch_0?_encoding=UTF8&qid=&sr=)

## The Obstacle Is the Way: The Timeless Art of Turning Trials into Triumph
### Sometimes the most important thing you can do is take a a step back and look at the world from a different perspective.
[Audible](https://www.audible.com/pd/The-Obstacle-Is-the-Way-Audiobook/B0BT54XTLW?source_code=ASSOR150021921000V)

## Project Hail Mary
[Audible](https://www.audible.com/pd/Project-Hail-Mary-Audiobook/B08G9PRS1K?source_code=ASSOR150021921000V)

## The Martian
[Audible](https://www.audible.com/pd/The-Martian-Audiobook/B082BHJMFF?source_code=ASSOR150021921000V)

## Setting the Table: The Transforming Power of Hospitality in Business
[Audible](https://www.audible.com/pd/Setting-the-Table-Audiobook/B002V5BQVY?source_code=ASSOR150021921000V)

## The Infinite Game
[Audible](https://www.audible.com/pd/The-Infinite-Game-Audiobook/B07DKHSL3W?source_code=ASSOR150021921000V)

## Creativity, Inc.: Overcoming the Unseen Forces That Stand in the Way of True Inspiration
[Audible](https://www.audible.com/pd/Creativity-Inc-Audiobook/B00IPK3BWQ?source_code=ASSOR150021921000V)

## Tao Te Ching
[Audible](https://www.audible.com/pd/Tao-Te-Ching-Audiobook/B01LXFJNTB?source_code=ASSOR150021921000V)

## Clean Code: A Handbook of Agile Software Craftsmanship
[Audible](https://www.audible.com/pd/Clean-Code-Audiobook/B08X7KL3TF?source_code=ASSOR150021921000V)

## The Hitchhiker's Guide to the Galaxy
[Audible](https://www.audible.com/pd/The-Hitchhikers-Guide-to-the-Galaxy-Audiobook/B002VA9SWS?source_code=ASSOR150021921000V)

## Fundamentals of Software Architecture: An Engineering Approach
[Audible](https://www.audible.com/pd/Fundamentals-of-Software-Architecture-Audiobook/B08X917VLR?source_code=ASSOR150021921000V)

## Site Reliability Engineering
[Audible](https://www.audible.com/pd/Site-Reliability-Engineering-Audiobook/B08VKYWGYD) 

[Free Online](https://sre.google/sre-book/table-of-contents/)

## Never Split the Difference: Negotiating as if Your Life Depended on It
[Audible](https://www.audible.com/pd/Never-Split-the-Difference-Audiobook/B01CF5O89G?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Indistractable: How to Control Your Attention and Choose Your Life
[Audible](https://www.audible.com/pd/Indistractable-Audiobook/B07SXBB3FS?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win 5th Anniversary Edition
[Audible](https://www.audible.com/pd/The-Phoenix-Project-Audiobook/B00VAZZY32?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## The Pragmatic Programmer: 20th Anniversary Edition, 2nd Edition
[Audible](https://www.audible.com/pd/The-Pragmatic-Programmer-20th-Anniversary-Edition-2nd-Edition-Audiobook/B0833FMYH9)

## It Doesn't Have to Be Crazy at Work
[Audible](https://www.audible.com/pd/It-Doesnt-Have-to-Be-Crazy-at-Work-Audiobook/0062877089?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Software Engineering at Google: Lessons Learned from Programming Over Time
[Audible](https://www.audible.com/pd/Software-Engineering-at-Google-Audiobook/B08VLS9Y95?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Monolith to Microservices: Evolutionary Patterns to Transform Your Monolith
[Audible](https://www.audible.com/pd/Monolith-to-Microservices-Audiobook/B08X7CJY9Y)

## A World Without Email: Reimagining Work in an Age of Communication Overload
[Audible](https://www.audible.com/pd/A-World-Without-Email-Audiobook/0525643575)

## Start with Why: How Great Leaders Inspire Everyone to Take Action
[Audible](https://www.audible.com/pd/Start-with-Why-Audiobook/B074VDVHZ5?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## The Complete Software Developer's Career Guide: How to Learn Programming Languages Quickly, Ace Your Programming Interview, and Land Your Software Developer Dream Job
[Audible](https://www.amazon.com/Complete-Software-Developers-Career-Guide/dp/B078J67VNF/ref=sr_1_1?crid=6M4AM60NTLLY&keywords=The+Complete+Software+Developer%27s+Career+Guide+Audible&qid=1664337054&qu=eyJxc2MiOiItMC4wMSIsInFzYSI6IjAuMDAiLCJxc3AiOiIwLjAwIn0=&s=audible&sprefix=the+complete+software+developer%27s+career+guide+audible,audible,273&sr=1-1)

## Made to Stick: Why Some Ideas Survive and Others Die
[Audible](https://www.audible.com/pd/Made-to-Stick-Audiobook/B002V0QVY6?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Hooked: How to Build Habit-Forming Products
[Audible](https://www.audible.com/pd/Hooked-How-to-Build-Habit-Forming-Products-Audiobook/B00HZNTRE0?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Chasing Excellence: A Story About Building the World's Fittest Athletes
[Audible](https://www.audible.com/pd/Chasing-Excellence-A-Story-About-Building-the-Worlds-Fittest-Athletes-Audiobook/B07843X52P?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Remote: Office Not Required
[Audible](https://www.audible.com/pd/Remote-Audiobook/B00DJ5W592?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Company of One: Why Staying Small Is the Next Big Thing for Business
[Audible](https://www.audible.com/pd/Company-of-One-Audiobook/B07KFN2255?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Tools of Titans: The Tactics, Routines, and Habits of Billionaires, Icons, and World-Class Performers
[Audible](https://www.audible.com/pd/Tools-of-Titans-Audiobook/B082VK6Q8G?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## The Alliance: Managing Talent in the Networked Age
[Audible](https://www.audible.com/pd/The-Alliance-Audiobook/B00LFS7MFM?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## The 4-Hour Workweek: Escape 9-5, Live Anywhere, and Join the New Rich (Expanded and Updated)
[Audible](https://www.audible.com/pd/The-4-Hour-Workweek-Escape-9-5-Live-Anywhere-and-Join-the-New-Rich-Expanded-and-Updated-Audiobook/B0031AS3BE?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## How to Win Friends and Influence People in the Digital Age
[Audible](https://www.audible.com/pd/How-to-Win-Friends-and-Influence-People-in-the-Digital-Age-Audiobook/B005MKCFCE?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## No Rules Rules: Netflix and the Culture of Reinvention
[Audible](https://www.audible.com/pd/No-Rules-Rules-Audiobook/1984891170?action_code=ASSGB149080119000H&share_location=pdp&shareTest=TestShare)

## Small Giants: Companies That Choose to Be Great Instead of Big, 10th Anniversary Edition
[Audible](https://www.audible.com/pd/Small-Giants-Audiobook/B01LVZXP8C)

# The Meh:
These are books I have read and found useful but am unlikely to ever read again.

## The $100 Startup: Reinvent the Way You Make a Living, Do What You Love, and Create a New Future
[Audible](https://www.audible.com/pd/The-100-Startup-Audiobook/B008165WSK?source_code=ASSOR150021921000V)

## The Million-Dollar, One-Person Business: Make Great Money. Work the Way You Like. Have the Life You Want.
[Audible](https://www.audible.com/pd/The-Million-Dollar-One-Person-Business-Audiobook/B077SMFHSV?source_code=ASSOR150021921000V)


## The Steal Like an Artist Audio Trilogy: How to Be Creative, Show Your Work, and Keep Going
[Audible](https://www.audible.com/pd/The-Steal-Like-an-Artist-Audio-Trilogy-Audiobook/B0B6QGTNVS?qid=1684018011&sr=1-1&ref=a_search_c3_lProduct_1_1&pf_rd_p=83218cca-c308-412f-bfcf-90198b687a2f&pf_rd_r=3DP36GMC576FPSAFN0QT&pageLoadId=ihPDbUIkzZ5VTfDn&creativeId=0d6f6720-f41c-457e-a42b-8c8dceb62f2c)

## The Laptop Millionaire: How Anyone Can Escape the 9 to 5 and Make Money Online
[Audible](https://www.audible.com/pd/The-Laptop-Millionaire-Audiobook/B0082FHBWU)

## The Art of Making Sh!t Up: Using the Principles of Improv to Become an Unstoppable Powerhouse
[Audible](https://www.audible.com/pd/The-Art-of-Making-Sht-Up-Audiobook/1469075733)

## The Membership Economy: Find Your Super Users, Master the Forever Transaction, and Build Recurring Revenue
[Audible](https://www.audible.com/pd/The-Membership-Economy-Audiobook/B01DKSOW3O)

## Measure What Matters: How Google, Bono, and the Gates Foundation Rock the World with OKRs
[Audible](https://www.audible.com/pd/Measure-What-Matters-Audiobook/B07BMHFBCM)

## Killing Sacred Cows: Overcoming the Financial Myths That Are Destroying Your Prosperity
[Audible](https://www.audible.com/pd/Killing-Sacred-Cows-Audiobook/B06VSY2DZS)

## Trump: The Art of the Deal
[Audible](https://www.audible.com/pd/Trump-The-Art-of-the-Deal-Audiobook/B01FY3KMKK)

## Walden: Life in the Woods
[Audible](https://www.audible.com/pd/Walden-Audiobook/B002V59WIS)

## Vagabonding: An Uncommon Guide to the Art of Long-Term World Travel
[Audible](https://www.audible.com/pd/Vagabonding-Audiobook/B00GCHWZHG)

## The Four Agreements
[Audible](https://www.audible.com/pd/The-Four-Agreements-Audiobook/B002VA3GJO)

## The Monk and the Riddle: The Art of Creating a Life While Making a Living
[Audible](https://www.audible.com/pd/The-Monk-and-the-Riddle-Audiobook/B07DDNPHJC)

## The Most Successful Small Business in the World: The Ten Principles
[Audible](https://www.audible.com/pd/The-Most-Successful-Small-Business-in-the-World-Audiobook/B0030EYAEK?source_code=ASSOR150021921000V)

# The Ugly:
These books weren't worth finishing.

## The Entrepreneur Roller Coaster
By Darren Hardy

## The 80/20 Principle 
By Richard Koch

