# 10/6/2023
"The Infinite Game": Learn a different way to think of business that will help your company keep playing when your rivals simply can’t.

## Get it on [Audible](https://www.amazon.com/The-Infinite-Game-Simon-Sinek-audiobook/dp/B07DKHFTB7/ref=tmm_aud_swatch_0?_encoding=UTF8&qid=&sr=)

## Book List Copy
📚 Discover a world of knowledge with my curated book list! 🌟 Dive into inspiring reads that'll ignite your curiosity and personal growth. Explore these must-reads today! https://rjmccii.gumroad.com/l/fxxtxi 

#BookList #ReadingList

# 10/13/2023
At some point during my University years I caught something terrible that I have never been able to sake: A Love of Learning!

Back when I was an engineer the constant churn of tech was enough to keep me sated but I found myself at a loss when I set aside my IDE for QBR’s. I solved the problem when I combined my daily walks with audiobooks and have for the last several years really enjoyed some great works. 

May I be so bold as to recommend a book for you?

"The Obstacle Is the Way: The Timeless Art of Turning Trials into Triumph": Sometimes the most important thing you can do is take a step back and look at life from a different perspective.

Give it a read, you’ll thank me later.

#MustReads #thegoodthemehandtheugly


## Get it on [Audible](https://www.audible.com/pd/The-Obstacle-Is-the-Way-Audiobook/B0BT54XTLW?source_code=ASSOR150021921000V)

## Book List Copy
📚 Discover a world of knowledge with my curated book list! 🌟 Dive into inspiring reads that'll ignite your curiosity and personal growth. Explore these must-reads today! https://rjmccii.gumroad.com/l/fxxtxi 

#BookList #ReadingList

# 10/20/2023
At some point during my University years I caught something terrible that I have never been able to sake: A Love of Learning!

Back when I was an engineer the constant churn of tech was enough to keep me sated but I found myself at a loss when I set aside my IDE for QBR’s. I solved the problem when I combined my daily walks with audiobooks and have for the last several years really enjoyed some great works. 

May I be so bold as to recommend a book for you?

"The Obstacle Is the Way: The Timeless Art of Turning Trials into Triumph": Sometimes the most important thing you can do is take a step back and look at life from a different perspective.

Give it a read, you’ll thank me later.

#MustReads #thegoodthemehandtheugly


## Get it on [Audible](https://www.audible.com/pd/The-Obstacle-Is-the-Way-Audiobook/B0BT54XTLW?source_code=ASSOR150021921000V)

## Book List Copy
📚 Discover a world of knowledge with my curated book list! 🌟 Dive into inspiring reads that'll ignite your curiosity and personal growth. Explore these must-reads today! https://rjmccii.gumroad.com/l/fxxtxi 

#BookList #ReadingList

# 11/3/2023
"The Infinite Game": Learn a different way to think of business that will help your company keep playing when your rivals simply can’t.

## Get it on [Audible](https://www.amazon.com/The-Infinite-Game-Simon-Sinek-audiobook/dp/B07DKHFTB7/ref=tmm_aud_swatch_0?_encoding=UTF8&qid=&sr=)

## Book List Copy
📖 Elevate your reading game with my handpicked book list! 🚀 Explore a collection of thought-provoking, life-changing books that will reshape your perspective. Check out my recommendations now! https://rjmccii.gumroad.com/l/fxxtxi

#BookRecommendations #MustReads

# 11/10/2023
At some point during my University years I caught something terrible that I have never been able to sake: A Love of Learning!

Back when I was an engineer the constant churn of tech was enough to keep me sated but I found myself at a loss when I set aside my IDE for QBR’s. I solved the problem when I combined my daily walks with audiobooks and have for the last several years really enjoyed some great works. 

May I be so bold as to recommend a book for you?

"The Obstacle Is the Way: The Timeless Art of Turning Trials into Triumph": Sometimes the most important thing you can do is take a step back and look at life from a different perspective.

Give it a read, you’ll thank me later.

#MustReads #thegoodthemehandtheugly

## Get it on [Audible](https://www.audible.com/pd/The-Obstacle-Is-the-Way-Audiobook/B0BT54XTLW?source_code=ASSOR150021921000V)

## Book List Copy
📖 Elevate your reading game with my handpicked book list! 🚀 Explore a collection of thought-provoking, life-changing books that will reshape your perspective. Check out my recommendations now! https://rjmccii.gumroad.com/l/fxxtxi

#BookRecommendations #MustReads

# 12/1/2023
"The Infinite Game": Learn a different way to think of business that will help your company keep playing when your rivals simply can’t.

## Get it on [Audible](https://www.amazon.com/The-Infinite-Game-Simon-Sinek-audiobook/dp/B07DKHFTB7/ref=tmm_aud_swatch_0?_encoding=UTF8&qid=&sr=)

## Book List Copy
📕 Looking for your next great read? 🌈 Explore my carefully curated book list featuring timeless classics and contemporary gems. Dive into a world of inspiration and knowledge! Don't miss out – start your reading journey today. https://rjmccii.gumroad.com/l/fxxtxi
 #ReadingJourney #BookLover

 # 12/8/2023
 t some point during my University years I caught something terrible that I have never been able to sake: A Love of Learning!

Back when I was an engineer the constant churn of tech was enough to keep me sated but I found myself at a loss when I set aside my IDE for QBR’s. I solved the problem when I combined my daily walks with audiobooks and have for the last several years really enjoyed some great works. 

May I be so bold as to recommend a book for you?

"The Obstacle Is the Way: The Timeless Art of Turning Trials into Triumph": Sometimes the most important thing you can do is take a step back and look at life from a different perspective.

Give it a read, you’ll thank me later.

#MustReads #thegoodthemehandtheugly

## Get it on [Audible](https://www.audible.com/pd/The-Obstacle-Is-the-Way-Audiobook/B0BT54XTLW?source_code=ASSOR150021921000V)

## Book List Copy
📕 Looking for your next great read? 🌈 Explore my carefully curated book list featuring timeless classics and contemporary gems. Dive into a world of inspiration and knowledge! Don't miss out – start your reading journey today. https://rjmccii.gumroad.com/l/fxxtxi
 #ReadingJourney #BookLover