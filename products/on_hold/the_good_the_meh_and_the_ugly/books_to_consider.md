Clean Agile - Back to basics by Robert C. Martin
Team Topologies Matthew Skelton and Manuel Pais
The Value Flywheel Effect (David Anderson)
Docs for Developers (Jared Bhatti)
The Fearless Organization (Amy Edmondson)
The Unicorn Project (Gene Kim)
Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation Jez Humble and David Farley
