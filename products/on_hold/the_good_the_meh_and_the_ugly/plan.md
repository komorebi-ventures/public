# How do I provide more value with this offering? 
- A 140-character or less book review for each (+1) 
    - Could be used as a weekly ad copy for the booklist. 
- A one-paragraph description of the book and why it's excellent (+1) 
- A fourth list of books that are currently in my queue to be read (+1)
- An extract of the ideas I found the best in the book (+1)
- A 1-page summary of every book on the Good List ($28)
    - Video Series  (???)
- A place to talk with others about these books and what you have learned from them. (Not sure how to price this, but it will be a subscription)
    - Allow the community to add books and reviews and vote on a book being on the List 
- Categorizing the Good List by Topic (+1)
- Sell a list for different specialties (i.e. one for software engineering, one for managment, one for company building, ect.)
- Sell a book-a-month club ($14 + 20%)
- Create courses based on the book ($20/each)

# How do I derive value from it?
- Reading and making these things will improve my overall learning, which is a big win.
    - I really enjoy studying and learning from these books. So worst case scinario its still a good hoby. I don't want to leave it at that.
- Direct sells 
- Become an Amazon affiliate
    - I am worried about this. I want to avoid supporting my work with advertisements because that relationship can skew the value provided to my fans. After all, I am trying to manage two relationships. If I do this one, it will need to be with care of, and I would need to end it if I found myself focusing on what Amazon needs rather than what my fans need.

# How do I market it? 
- I can use the 140-character review can be used for advertising
	- Use LinkedIn and make this a regular feature on the platform.
	- When I get to Twitter, this will work for that too

# What I won't do.
- I won't become an Amazon affiliate while I am activly selling the book list. They are paying me to do this work, and I don't want to be in a position where I am trying to sell them something while I am trying to sell them something else.

# Order of Operation
- Get the 140 charchater descriptions written for each book
- weekly start posting one of the book descriputions to linkedin, including a link to buy the book, and a link to the book list in the thread. 
- Start relistening to and reading the list. Start with just Made to Stick, Hooked, Indistractable, and Automic Habbits. For each 
  - take notes and store them in the repo
  - Write the one paragraph summary
  - Write the bullitpoint list of the best ideas in the book
  - Write the One Page Summary
- Continue Posting The Descriptions Weekly While I work on the Habbit builder app. 
- Each week learn we I can from the sales.
- When the habbit builder app is in the store come back to this and see if their is value in continueing the process through the whole list. 