# Local Enviornment Setup
It is assumed you are working in Ubuntu 22.04 LTS. 
run the following commands:
## [Install Flutter & Dart](https://docs.flutter.dev/get-started/install/linux)
- `sudo snap install curl`
- `sudo apt install git`
- `sudo snap install flutter --classic`
- `flutter doctor`
- `sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386`