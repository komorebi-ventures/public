I am greatfull for family.

#givethanks

Posted on 8/13/24: https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7229124575173689345-TQF2?utm_source=share&utm_medium=member_desktop 

---

Hussein Nasser advice resonates strongly with me: When troubleshooting system performance issues, don't forget to verify that the database is returning only the data you need. (Of course, engineering it that way from the beginning is even better!)

In 2021, we experienced an unexpected system slowdown causing significant issues for our clients. The culprit? A seemingly innocuous piece of code that had worked flawlessly for years. It was returning all fields and records from a table, but our user base had grown 100-fold since its implementation.

Some users had gotten creative with data storage, with one even inserting the entire text of "War and Peace" into a single field. Consequently, every query (occurring hundreds of times per minute) was returning far more data than necessary. The fix was simple, but it highlights a crucial area to investigate when facing unexpected performance issues without recent code changes.

Posted on 8/14/24: https://www.linkedin.com/posts/robertmccleary_try-avoiding-select-even-on-single-column-activity-7229486976490463233-Cgbi?utm_source=share&utm_medium=member_desktop

---
Quality is free, but only to those who are willing to pay heavily for it.
-DeMarco and Lister


taken from: https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr
Posted on 8/15/24: https://www.linkedin.com/posts/robertmccleary_quality-is-free-but-only-to-those-who-are-activity-7229849363366211584-Gx3H?utm_source=share&utm_medium=member_desktop 

---
A COBOL programmer, after making his fortune from Y2K remediation, had himself cryogenically frozen. His directive: Wake him when faster-than-light travel was invented so he could explore the stars.

Upon awakening, he excitedly asked the doctor, "Where has humanity gone now that we can travel the cosmos?"

The doctor replied, "We haven't mastered FTL yet. We woke you because it's the year 9999, and you know COBOL."

Adapted from: https://www.databasestar.com/programmer-jokes/ 

Posted on 8/16/24: https://www.linkedin.com/posts/robertmccleary_fridayfunnies-activity-7230211747297550336-C4sU?utm_source=share&utm_medium=member_desktop
