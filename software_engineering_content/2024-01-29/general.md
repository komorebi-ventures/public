I am grateful that there are people who rely on me.

#givethanks

Posted On 1/30/2024: https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7158283215776935936-ImbZ?utm_source=share&utm_medium=member_desktop 

---

A primary cause of complexity is that software vendors uncritically adopt almost any feature that users want.

--Niklaus Wirth

Taken From: https://www.comp.nus.edu.sg/~damithch/pages/SE-quotes.htm

Posted on 2/1/2024: https://www.linkedin.com/posts/robertmccleary_a-primary-cause-of-complexity-is-that-software-activity-7158971026221973504-YBqL?utm_source=share&utm_medium=member_desktop 

