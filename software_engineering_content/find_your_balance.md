I am going to let you in on a little secret of mine. I struggle to stay balanced. 
It's been that way my whole life. It's been better since I recovered from my burnout, but it is still a battle I fight. 
In the last few months, I have tipped things out of balance. I know I can't keep it up. Without a change, I'll find myself back in the burnout zone and will not be able to live the life I want.
The rest of this is an open letter to myself. I used it to remind myself about what I know and to encourage myself to get through this challenging moment.
I hope it helps you too.
----------------------------------------------------------------------------
Hey Robert, 
I am a little worried about you. You have lost your balance these last few months, and I can tell you're hurting because of it. 
I hope you will take this as kindness and that you won't judge us harshly because I am candidly pointing out our shortcomings.
It's time to put things back in order and live a more extraordinary life.
Don't Forget the Basics 
I know how you think and what you want. You want to take care of your family, employer, and team. You want to have an outsized impact on everything you touch, but if you keep going in this direction, you will not be able to.
You won't accomplish anything if you are sick or tired or your strength fails. It is not weak or selfish to make sure you have what you need to be your best. 
You can't get water from a dry well.
You can't save someone else when you are drowning.
You have got to get back into the habit of living a healthy lifestyle.
Start scheduling dedicated time to eat well, drink clean water, get enough sleep and exercise, and tend to your relationships. Those are everyday things that you can not afford to neglect. 
We know you are trapped in an addictive cycle with your work and play in technology. With all of your other additions, you got help and now avoid them, but even though the life of a wild hermit may sound appealing to you, it isn't the life you want to live. 
To make things work, you must take care of a couple of extra things. You need to put your 12-step program back on your schedule and not let it take a backseat anymore. Working the steps and talking with others on a similar journey goes a long way in strengthening you for the change. 
Finally, find a new therapist and get that back on a regular schedule. Your work there has made a real improvement in your quality of life. I have no idea how much more progress you can make, but even a marginal gain in your mental health can have an outsized impact on your happiness.
Reset boundaries with yourself, your manager, and your tech
Planning to take care of yourself is the first step, but other aspects of your life will consume everything without good boundaries.
Decide how much work you can do in a single day without losing the ability to care for the rest of you, and let that be your limit. 
I know you lead an SRE team, and emergency operations and management are part of your job. When emergencies strike, ensure you are adjusting your work time in other ways to ensure you are taking care of your regular healthy activities. 
Sleep is the only need where this strategy can't work. I am unsure what else you can do without hiring a globally distributed team. Keep looking for ways to improve this, and make sure you aren't making the sleep interruption aspect of your job worse by not taking advantage of the opportunity to sleep well when available.
Your phone can access work email, but you should stop checking it when you're away from the office. Keep things set on your phone, so email and social media don't notify you. You should choose when and if to interact with those technologies, not the other way around. Slack is a significant component of how your company communicates. Set Slack to notify you only when someone is speaking to you directly. Additionally, cancel notifications and don't check in when taking time off. 
Negotiate the Most Valuable Use of Your Time and Let the Rest Go
You learned early in your career that the more you get done, the more there is to do. Don't forget you can't do everything. 
You also know you shouldn't even try to do it all. Not every task has the same value, and taking the time to work on the most valuable thing ensures the best possible outcome from your work.
In the last nine or ten months, I have noticed that you have fallen into the habit of trying to do it all. You accept every assignment and don't let your team take care of the things they can without you. 
You are making a mistake and setting the wrong example for your team. You must ensure that you select the most valuable work and negotiate the rest off your plate. It would be best if you did the same for the work you give to your department and teach them this skill too.
Set Aside Plenty of Time to Accomplish Valuable Work
Even though most of your job is coordinating and communicating, you need plenty of time to focus on doing that well. 
When you were still a hands-on keyboard engineer, you set aside most of the time on your calendar to focus on accomplishing real work. You need to return to that. 
One way to help you here is you need to get way pickier about what meetings you are attending. I know your FOMO is driving you to accept every invite right now. You are hoping you can be a part of all the critical decisions by being everywhere, but all it is doing is keeping your attention spread too thin.
Some meetings effectively use your time, but not all of them do. You have been at companies that worship at the altar of the eternal meeting, and as a result, they accomplish nearly nothing. Don't let that happen here. 
It would help if you started asking for agendas with straightforward questions to answer or decisions to make. Anywhere possible, you should be answering those questions and making those decisions asynchronously. 
It is also beyond time to assign areas of responsibility to members of your department. They should report their conclusions to you, but make sure they are empowered to decide and act. Stop being the dang bottleneck. 
Make PTO a Meaningful Part of Your Work Cycle
Of everything I have called out, you are pretty good at taking care of this one. Continue to actively make paid time off a part of your work cycle. Sometimes it's as simple as being a WATCHDOG for the day at your kid's school, but often you need some extended time away from work to stay sharp and prevent burnout.
Your wife thinks your extended breaks will be much more effective if you disconnect from all electronics. I think she is right. At the end of the quarter, when you take your next PTO, you should humble yourself and accept her advice. I would bet cash it will 10x the effectiveness of your time away.
Find a Way to Detach From Work at the End of the Day
Your stress from work is leaking into and souring every other aspect of your life. Look for a way to detach from work at the end of the day. 
You might try exercise right after work and end that with a meditation session. Get it done before you "go home"; maybe that will be enough.
Listen, I love you. I am proud of you. You take care of your team, company, and family admirably. If you can get back to taking care of yourself, you will be able to take better care of all the rest, enjoy it a lot more, and sustain it over the long term. 
All the best
Me