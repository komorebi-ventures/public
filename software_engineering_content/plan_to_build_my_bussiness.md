# Background and The goal.
I am a 42-year-old man who has spent most of his time since 1995 neck deep in the modern computing era. I have deep experience in DevOps, SRE, and people management. I have light experience in software development,  software engineering and software sales but a high interest in software engineering. I have fourteen years of experience working at software companies. More than half of that experience comes from remote positions at small to medium-sized tech startups. I lost my job in 2023 and need to find a way to replace my previous income.

I currently have two temporary jobs that provide about half of my childrens and I’s needs. I am currently looking for a new opportunity in my field of expertise to close the gap. My biggest challenge thus far is not being technical enough for the positions that I apply to. Most CTO/VP/Director of engineering positions I find most interesting (ie at startups) require a technical interview, and I haven't been able to pass one since I started looking.

Ultimately my goal is to provide myself with financial and time freedom so that I can be present in my childrens lives far beyond anything I have done in the past. I want to own a business that will provide significant returns (i.e., between $6,000 and $35,000/month) but requires at most 10 hours per week to maintain it once built. Additionally, the business should be able to be managed from anywhere on earth with a solid internet connection. The products should be quick to test for success and be capable of fulfilling my target income by the 7th month of operation. They should be something I would personally buy or use. I should be able to encapsulate each product's benefits in a single sentence. I should be able to sell the products for between $50-$200. Each should be fully explainable with an excellent online FAQ. The products should not be objectionable to my morals, such as gambling or pornography.

My new plan is this. Continue to apply for the full time positions that have provided me with my experience thus far. Start alternating between building software I will personally use and information products based on my expertise in building and scaling the DevOps and SRE functions at startups. Alternate my newsletters to be a series of posts based on the product I am working on currently. Either delving into the low level details or sharing a good portion of the information product as content first before releasing the product in a condensed easy to consume package. Take the newsletter topics and break them out into multiple bite sized general posts. When one resonates, plug myself for consulting services. When I start to see a trend of customer needs, start building a product that meets those needs and continue the feedback loop.


# The Plan

## Find a new job at a respectable company where I can provide outsized value and where the whole team has my back.

Find and apply for one job each week.

Build software or do leetcode challenges every day to get my technical skills back in top form.


## Build a business that will help me reach my goals. 

See if I can find a platform that has consulting opportunities, and if I find one apply for one opportunity each week.

Alternate between building software products that I need and building information products based on my expertise. 

## Change my LinkedIn content marketing strategy

As I build my software products, I write about the experience and share it on LinkedIn. Focus on long form newsletter articles that help others facing similar challenges, build a following, and establish expertise.

When I am building information products, I build them and share as much as makes sense as long form newsletter articles broken into topics or chapters. 

In both cases turn the long form content into LinkedIn Standup Newsletter articles. Break the long form content up into small digestible chunks as posts with the goal of getting 10 small posts from each long form article. Point to the main article in the comments. Revamp those digestible chunks 10 times each and post them once a month.  One piece of good content then becomes hundreds of pieces of valuable content that all point to the big content. This should provide a good deal of value and establish authority in these areas of expertise. It's also giving me an opportunity to develop my voice and figure out what the best way to resonate with my audience is. 

### Plug Services and Products in the Newsletter

At the bottom of each newsletter plug my consulting services, Newsletter Sponsorship and my latest product. 

### Consistently Ask for Commitment
Once a week ask the folks who reacted or commented on my best performing piece of content to follow me, or sign up for my newsletter. Something small and free but that increases their connection with me.

### Support other creators
Once a week I find another creator I value, analyze and break down something they created and share it on LinkedIn. This will give my followers value, and the other creator value.

Creators I can do this for:
- Hussein Nasser ++ 
- Erik Andersen
- Marc van Neerven +
- Nir Eyal +
- Jason Fried
- Wes Cooper
- Simon Sinek
- David Heinemeier Hansson
- Codie Sanchez
- John Crickett +
- Sahil Bloom
- Andrew Fong
- Mike Thornton
- Roman Frolov

### Content to emulate
- https://www.linkedin.com/posts/jason-fried_youll-often-hear-people-say-someone-has-activity-7121222737276141568-_hVq?utm_source=share&utm_medium=member_desktop 
- https://www.linkedin.com/posts/samuel-szuchan_ive-never-met-my-team-were-fully-remote-activity-7121095268275486722-vvPu?utm_source=share&utm_medium=member_desktop 
- https://www.linkedin.com/posts/skills-software-development_as-a-software-developer-you-know-how-challenging-activity-7117939159993864192-AlXv?utm_source=share&utm_medium=member_desktop

### Weekly Content Plan
Monday through Saturday (When I don't have my kids) from 6PM - 7PM MT I am going to post something to LinkedIn and interact with folks who respond to my posts or to the great posts of people I am following. I will also post the short form content that plugs the newsletter. When I am working on leetcode problems I will post about my solutions as well. 

Monday - Newsletter  
Tuesday - Gratitude  
Wednesday - Plug someone else's great content  
Thursday - A great quote  
Friday - #FridayFunnies
Saturday - Newsletter Teaser 

# Products to Work On

## Software Products 

### A Communications Platform for Small Teams

MVP is a WYSIWYG HTML editor whose purpose is building simple web docs that are searchable.

## Information Products

### The DevOps/SRE/Engineering Leadership Essential Reading Lists

Take the time to read (or listen to) books related to things I am passionate about in my career.
- Building startups
- Software engineering
- Software architecture
- Systems theory
- SRE
- DevOps
- Team building
- People management
- high performance engineering teams
- remote first companies
- ect


To facilitate learning, take notes on each book. Create one page synopsis of each. Distill that down to a 140 character book review. Use those book reviews as simple content to talk about the books worth reading.

Once I have 5 books studied in the above fashion, create a new book list product and follow the plan setup for the good, the bad and the ugly list to ramp up my ability to earn a living from it.

### DevOps Adoption Guide
Read the 2023 DORA State of DevOps Report, Accelerate, The Phoenix Project, The Unicorn Project, The DevOps Handbook,  and Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation then research the various individual components in their recommendations. Building out a leadership strategy guide for small startups who want to start adopting DevOps.

As I research each of these items out I can build the first book list too. 

This can eventually turn into tactical guides to give the hands on folks clear direction to help their organizations adopt DevOps, and done for you technology setups that make adopting “best practices” push button easy.


## This is as far as I am currently going
---


# Connect with 3 people each month and tell them why I appreciate their work
When I find people who are doing great work and making an impact on me, be sure to tell them so. Spend time making sure they know they are making a difference in people's lives. 


# Twice each year setup a Sale
Create a limited time only sale or an invite only sale once every six months to improve the sales pipeline a couple times a year. 


# Start cross posting with other creators
Start offering opportunities to write guest posts, newsletters, or blog articles with the creators that I enjoy the most.

# Rebuild robertmccleary.com 
Use this to be the place where all my products are available (even if I am still selling through gumroad) and to keep a second copy of things in case I get deplatformed. 


# Hone in on the signals
Some topics I have already covered have far more engagement than anything else I have done. It's time to start drawing out ideas for future writing. Looking to build a feedback loop to improve engagement, followers and subscribers.


# Find my ideal customers
Once I have people who are paying me look for the ones I love working with. Figure out commonalities in who they are and what they need. As people are happy with my work ask them to write testimonials.

# Start hosting webinars and workshops
I can use these to test my product ideas in a more free form way and earn a higher return than my other time intensive options provide.

# Double my rates
Start building products that meet their needs specifically. Start creating content that will be valuable to that group.  double my consulting rates, focus on the niche, say no to working with folks outside of the niche. I need to ensure that what I am providing is valuable enough for the rates I am charging.


# Reduce the time I am spending on these things
Use the increased rate to keep the existing cash flow going but focus the new extra time on products that will scale my income without requiring my time and attention once set up. Build out solid online course options based on what I have learned from the books and the success of various posts and my finding the signals and my ideal customer profile.
 
# Keep my eyes and ears open
Look for patterns in what people are asking for that are not related to my niche and can provide a way for me to build a completely separate stream of income.


# Test products looking for good fits
Run tests on MVP type products looking for good reception and when I find one double down to maximize the return I get from the product. 


# Triple down
Once I have a product that does really well, when sales begin to die down or the content begins to age out of relevance revamp it making it up to date and far more valuable and triple the price. 


# Create an army of happy customers
In the stream of using the product make sure I am asking for people to leave a testimonial and sign up for an affiliate program. If the product is a course, ask them again at the end of the course. 


# Build out a community
Create a community (mini social network) exclusively for my happy paying customers where I can provide value and they can add to that value with their own experiences and conversations. Might need to rethink this one if I have to be in it 24x7 to keep it working.


# Expand my efforts to a second social network
Once I have a steady stream of income from my products, I start working on twitter for the same effect. 


# Find ways to change what I am already doing into additional products
I will have built out systems to make things easier for me to complete the work I am doing and find the way to sell this work I am already doing for another source of income.


# Play the infinite game
I won't make millions overnight, but play the infinite game, shift my attention as it makes sense, enjoy what I am doing and be happy in both my successes and losses. The point isn't to be the best, or the biggest, but to provide real value and to be able to keep playing the game as long as I want to and make it an enjoyable part of my life. 

---
This plan is partially based on 
https://www.linkedin.com/posts/justinwelsh_20-steps-to-4m-in-revenue-activity-7066748100714852352-Jzv1/?utm_source=share&utm_medium=member_desktop 









