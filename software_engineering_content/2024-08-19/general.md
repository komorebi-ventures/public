I am grateful for the smell of rain.

What scents are you grateful for today?

#GiveThanks #SimpleJoys

P.S. 

There's a beautiful word for this scent: "petrichor." It's the earthy fragrance produced when rain falls on dry soil. Nature's own perfume, it always makes me feel refreshed and invigorated.

A big thank you to Doctor Who for introducing me to this lovely word!

---
Erik Andersen brings up a great point: too many of us live in fear, and his recommendation of reframing whatever we're afraid of is a really powerful tool.

A great tool I picked up from a Tony Robbins seminar is in the power of gratitude. If you can focus your mind on a memory that fills you with gratitude and keep yourself focused on feeling grateful for what you heard, saw, or did in that moment even for just a few minutes, fear (and anger) will actually be displaced by your gratitude. It's an excellent way of getting back into a place where you can make your best decisions.

What techniques do you use to overcome fear and make better choices?

#OvercomingFear #Gratitude #PersonalGrowth

P.S. If you're on the fence about doing a Tony Robbins Seminar, get off and do it. It's money well spent.


https://www.linkedin.com/posts/ebandersen_mentalhealth-coding-programming-activity-7229147421887385601-Oa66?utm_source=share&utm_medium=member_desktop

---

Computer science education cannot make anybody an expert programmer any more than studying brushes and pigment can make somebody an expert painter.

--by Eric S. Raymond.

From: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66 

---

A QA engineer walks into a bar and orders a beer, -1 beer, 50 beers, foobar beers, and NULL beers. The barman happily fulfills the orders he can and declines the rest. 



The QA engineer drinks his beers and leaves the bar with a satisfied smile on his face.



A user walks into the same bar and asks the barman where the toilets are. 



The barman explodes, the bar burns to the ground, and the building collapses.



#FridayFunnies #SoftwareEngineeringLife

adapted from
https://upjoke.com/software-jokes

---

The stories you tell yourself matter. 

The difference between telling yourself, "I have to go to work today" and "I get to go to work today" is palpable. 

The story you tell yourself is the difference between finding solutions or spinning your wheels. 

The story you tell yourself is the difference between feeling like you just wasted the day or feeling like you accomplished something important. 

Ultimately, both perspectives may be accurate, but I find the latter far more helpful. 

You decide how your story is told, make it a good one!