🚀 Have you been dreaming of something that stays on your pillow when you get when you get up every day?

Living your dreams really won’t be as hard as you think, because it will be more difficult than anything you can imagine.

8 years ago I decided I wanted to spend less time commuting and traveling and more time with my family by working remotely. I was traveling 70% of the time then when I wasn’t I spent an hour and a half each way commuting. It was simply too much. I started having conversations with my bosses, then started to look for a remote position, and it only took a few months to secure a remote job, move my family to our ideal location, and start living the dream. It took alot to get used to it and was hard in a completely different way than anything I had imagined before but I will never regret making the choice to have more time with them. 

My question for you now is what are you going to do today to make your dream a reality? Share in the comments what you're going to do today to make your dream real, and let's inspire each other to reach new heights! 

Did this help? Don't forget to share it with someone you know needs it too.
#SoftwareEngineering #DreamBig #CareerGoals

posted on 2/29/24 : https://www.linkedin.com/posts/robertmccleary_softwareengineering-dreambig-careergoals-activity-7169171759982661632-yyYq?utm_source=share&utm_medium=member_desktop 

---


