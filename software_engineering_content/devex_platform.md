# Items for a core DevEx Platform
- CI/CD Pipeline
- Developer Portal (something like backstage)

# Questions to answer for each
- What problems does it solve
- what tradeoffs are you making when you adopt it
- at what team scale does it make sense?