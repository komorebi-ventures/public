# You Should Practice Interviewing!

If you have any meaningful software engineering experience visible on your LinkedIn profile, you are being contacted by recruiters at least semi-regularly. Sometimes we find this contact annoying, especially if we are not interested in moving away from our current position. It is a waste, especially since we know that crickets are all we will hear from these folks when we actually need a new job. 

When thinking about situations like this, I like to consider a principle I borrow from the permaculture community: produce no waste. In permaculture, they are trying to mimic natural systems, and there is no waste in nature. One organism's waste is another's food, and the endless cycling of energy is very efficient. In a healthy permaculture system, every waste stream is viewed as a resource and put to productive use.
In our situation, we have waste coming from our LinkedIn profiles in the form of unsolicited and unwanted recruiter contact. Running with the idea of producing no waste, we should consider how to use these contacts for a productive end. 

One obvious way we can constructively use these interactions is by practicing our interviewing skills.  

Many of the recruiter contacts we get have already vetted your LinkedIn profile for a skills match, so for these, all you have to do is let the recruiter know you are interested in having a conversation, and BAAM! You have an instant opportunity to practice your interviewing skills. You might even proceed to the technical interview and get even more practice if you do well. 

It may not make sense to interview every time a recruiter contacts you. In that case, use the connection to expand or help your network. Passing their information to qualified friends and former colleagues might help people you already know and earn you a place in that recruiter's memory.
How often should you take an interview like this? I recommend you interview a minimum of once per quarter. 

TL;DR
Don't let the conacts from recuriters on LinkedIn go to waste, ocasionally take the oporunity to interview when the option arises. Keeping your interview skills sharp, building your network, and helping people you allready know will all serve you well when its actually time to move on to your job.  

Do you have a great way to take advantage of these types of recuiter contacts? I would love to read about it in the comments section!

---
"🚀 Ready to level up your professional journey? Our upcoming article reveals a game-changing approach to dealing with unsolicited recruiter messages on LinkedIn. Sign up for our newsletter and be the first to read it! #CareerTips #LinkedInHacks [NEWSLETTER SIGNUP LINK]"

"Think those LinkedIn messages from recruiters are a waste? Think again! I am dropping an article on Tuesday that explains it all. 

Sign up for the LinkedIn Standup today and don't miss this piece of career-altering advice. 

#CareerInsights #Newsletter [NEWSLETTER SIGNUP LINK]"

"💼 Unleash your career potential with our insider advice! Our next article explores an innovative way to use those recurring LinkedIn messages from recruiters to your advantage. Sign up for our newsletter and get it fresh off the press! #CareerDevelopment [NEWSLETTER SIGNUP LINK]"

"Can spam become success? Our upcoming article says YES! Find out how unsolicited LinkedIn messages can propel your career forward. Don’t miss out, subscribe to our newsletter today! #CareerHacks #LinkedInTips [NEWSLETTER SIGNUP LINK]"

---

"🔎 Ever find unsolicited recruiter contacts on LinkedIn annoying? Discover how to transform them into opportunities in our latest post! Practice your interview skills, expand your network, and more. 💼🤝 #Networking #CareerAdvice [LINK]"

"👩‍💻 Tired of recruiter spam on LinkedIn? In this weeks LinkedIn Standup offers a new perspective: Use them to your advantage! 

Read it now!

#SoftwareEngineering #CareerDevelopment"

"Do you often ignore those LinkedIn messages from recruiters? What if there's a way to turn them into valuable career building tools? Check out our latest blog post to find out how. #LinkedInTips #CareerGrowth [LINK]"

"Transform waste into wealth!💡Unsolicited LinkedIn messages from recruiters are not just spam, they're practice opportunities for interviews and networking! Learn more in our new blog post. #CareerHacks #ProfessionalGrowth [LINK]"