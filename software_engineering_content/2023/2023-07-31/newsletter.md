# The Top 10 Things You Can Do To Rock Out In Your Software Engineering Job

Do you want your work life to suck? 

No, you say??? 

Of course, you don't, but many times, we find ourselves living in that space without any thought about getting out.

Why, you may ask? We simply don't realize we can do anything about it. 

The truth is work is hard and it has to be, because if it weren't they wouldn't pay you to do it. 

That being said unless your dealing with a company or a boss that is truly sick, work is just what you make of it. You get to choose your adventure because you are the storyteller. 

In this week's LinkedIn Standup, we will cover some of the highest value recommendations I know of for taking control of your work life and making it the best it can be. Here are my top 10 things you can do to rock out your software engineering job. 

## #1 Learn to Give Up the Things You Can't Control
One of the biggest mistakes people make in life in general is believing that they are in control of everything. I have no idea how much of the world I control but my guess is if the world could be represented by all the sand on all the beaches I may control one grain of it.

If you want to find happiness at work you have to learn to let go of things you can't control. Does the latest employee handbook mention that your employment is "at will" 20 times? Does that make you feel like your company could care less about you as a person? Give the feedback to the right people in the right ways, but then let it go. Your emotions naturally flow from the circumstances around you, you should feel them but then you get to decide what you are going to do with them. In the case above letting it go just means accepting someone else has made a choice you don't appreciate and then letting the thoughts and feelings about it go so you can focus on things you can change, like the font size in your IDE.

## #2 Take Care of Yourself
This may not be obvious to you but a lot of how we feel is related to how we treat ourselves. So feeling and being your best at work starts before you are even at work. Take time to take care of yourself every day. Personally, my morning routine includes practicing my spirituality, exercising my body, feeding myself properly, and doing something creative (like this newsletter). When I do these things work feels better without anything else changing. 

You can help yourself keep the momentum by making sure you also take care of yourself during the workday. Take breaks to stretch, use the restroom, and disconnect from the code and the meetings long enough to feel fresher and ready for more.

## #3 Learn to do one thing at a time
In the modern workplace, we have a host of responsibilities at any given time. We are expected to diagram our architecture, understand user stories, write the code, attend meetings, and respond to people on Slack. Each one of these has a time and a place but where most of us need to improve is accepting that we can't do all of them at once. The best research I know of says that 98% of us can't do more than one thing well at a time. 

If you are in the 2% I am jealous and am not talking to you, but if you are like me then making work the best that it can be means looking at all the things you are expected to get done, planning time to do each of them and only doing one at a time. That one habit will make you more effective than you could have ever imagined and being effective and successful at what you set out to do has a huge tendency to make you feel good about work. 

## #4 Learn to Communicate
One thing that learning and writing computer languages all day doesn't make us good at is communicating with real human people. It is a priceless skill that will help you in far more than your work life but without it, you will struggle to make any kind of progress at work. 

Three tips for instantly improving your communication skills
- Seek to listen and understand before seeking to be understood. 
  - That may seem counterintuitive but you can't control if the other person wants to or is even capable of understanding you. You can on the other hand control if you're trying to understand them. When you are communicating with others spend most of your time listening, and asking clarifying questions to ensure you truly understand what they are saying before you attempt to get your point across. Doing this one thing can connect you to people in a stronger and more real way than virtually anything else. 
- Practice being clear and crisp in what you say and write
  - The biggest barrier for other people to understand you is the tendency we all have to fill our communication with the ephemera that our minds come up with. Take the time to ensure that everything you write is as clear and straightforward as you know how to make it. Also, practice this skill when you are speaking. It can be hard to keep yourself on track during an important conversation, but ensuring that you are clear and concise in what you say will help both you and your listener to feel like the time was well spent.
- Improve your non-verbal communication
  - As much as 55% of the information we convey during an interaction with someone else is through non-verbal means. Make sure when you can be in person or even on a Zoom call that your non-verbal communication matches what you're saying. Are you excited about a new project you will be working on together? Make sure that is visible on your face. Do you want to make sure that the other person knows you are paying attention to them? Then keep your eyes on them not focused on something off in the distance. 

## #5 Adopt the DevOps and Agile Philosophy of Continuous Improvement

Continuous improvement is the art of knowing where you are, where you want to go and making calculated changes to help you get there. 

This principle has helped our entire field progress at an astonishing speed and it can help you personally too.

## #6 Master Your Tools
Each software engineer has a toolkit - the IDE, version control system, debuggers, build tools, etc. Knowing your tools inside out will make the friction of completing your work each day low and the chances of completing your tasks high. Learn keyboard shortcuts, set up your IDE to your liking, understand the advanced features of your version control system, and automate tasks when it makes sense. Stay updated with the new features and improvements in the tools you use. Consider sharing useful tips and tricks with your team - everyone can benefit from this knowledge.

## #7 Foster a Great Work Environment
Software Engineering is a team sport. We rely on each other, other specialists, and other disciplines to get complex work done. Do your part to make the environment great. Be open and welcoming to new team members and those who ask questions. Find opportunities to spend some time getting to know one another as people rather than just colleagues. Learn to apply kindness concerning our patterns and processes, for example by not being a jerk when you are reviewing other people's code. 

Look for people who you can learn from (people who can be your mentors) and keep an eye out for people you can help (people you can mentor). Building a community in your team will go a long way toward work feeling like a great place to be.

## #8 Invest in Continuous Learning
This one goes hand in hand with suggestions like improving your communication skills, mastering your tooling, and developing a personal habit of continuous improvement but it still makes sense to call out all on its own. 

We can only draw on the experience we have. We can only write in languages that we know. We can only solve problems with what we know. Taking the time to regularly expand your experience, and knowledge makes you stronger and more resilient. It can and will make work better for you every day so make sure you have it on your schedule and even if the company isn't okay with you doing it on their time you should find an opportunity to learn elsewhere. 

## #9 Improve Documentation
Most of us do not enjoy documenting our code or our process. Good code is self-documenting, but there is one thing that the best code will never communicate on its own... "why ?". 

You see the reason you decided to write what you did the way that you did isn't communicated via code. It's easy to follow what clean well-written code does, but without understanding why it was written that way it's hard to change it safely. At a minimum take the extra time to document in your comments why you did things this way, it will both help you and others improve the code later which makes future workdays better. 

## #10 Don't believe everything that you think
Not everything that you think is true. Not everything that you make is good. Not everything that you do is flawless. Accepting this allows you to take advantage of correction that comes from people and experience. It also helps us to grow because as we continually improve and learn we can go back to old thoughts, code, and actions and realize how much we didn't understand, then make it better one day at a time. 

tl;dr

Most of the time our experience at work can be dramatically improved by accepting what we can't change and choosing to change what we can.

I hope this was helpful. Pick at least one item on the list and implement it today to rock out your job. Work will be better because you did.

These are my favorite tips but the list isn't exhaustive list or infallible. Do you have something that should be on the next list? Do you have a thought to improve something I mentioned? Reach out and let's have a conversation or start one in the comments, you'll be glad you did. 

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/does-engineer-morale-play-part-engineering-outcomes-robert-mccleary). 

---
Whenever you're ready, there are three ways I can help you: 

Consulting Services: Do you want 1:1 Advice about how to progress your career?  Book a [quick call](https://topmate.io/robert_mccleary/450908) with me today.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Gain insight from the books that have shaped my thinking. Buy [The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi) today! 

Maximize your brand visibility by sponsoring this newsletter. Connect with an engaged audience in the tech industry. [Set up a time today](https://topmate.io/robert_mccleary/450948) to discuss the opportunity.

---


# Teaser

🔥 Get Ready to Supercharge Your Software Engineering Career! 🔥

Hello, amazing tech professionals! 🙌

Are you ready to break the cycle of meh and breathe fresh life into your daily routine? Looking for a way to make your software engineering job not just good, but exceptional?

The next edition of The LinkedIn Standup Newsletter is about to drop, and you don't want to miss this one! We've crammed it full of insightful, actionable strategies that you can implement TODAY to rock out your software engineering job! 💻🚀

In it, we're giving you the exclusive "The Top 10 Things You Can Do To Rock Out Your Software Engineering Job" — a treasure trove of tips and tricks designed to boost your productivity, sharpen your skills, enhance your communication, and foster a brilliant work environment!

This is more than just a read; it's an experience that can catapult your career to new heights. 📈🌟

But here's the catch: this high-value content is exclusively for our newsletter subscribers. Not on our list? Don't worry; we've got you covered!

Click the link below to subscribe to The LinkedIn Standup Newsletter now, and make sure you're front and center when this game-changing content goes live on Saturday. 💌

Let's not just get through the workday; let's ROCK it! Sign up now, and let's take your software engineering job from good to extraordinary together. 💪💼

[Subscribe Now] Button

--- 

🚀Ready to amp up your software engineering game? 💻 Today's LinkedIn Standup Newsletter "The Top 10 Things You Can Do To Rock Out Your Software Engineering Job" is the ultimate guide to boost your career! Get practical tips to skyrocket your productivity, enhance your skills, and master your workspace! Read now and ROCK your workday ! ➡️ [Link]

😟 Feeling stuck in your software engineering job? We've got the perfect solution! Check out our article, "The Top 10 Things You Can Do To Rock Out Your Software Engineering Job". It's packed with actionable insights and tips you can implement right now to transform your job from boring to soaring! Dive in and discover how to supercharge your career! ➡️ [Link]

💡 Ever wondered how you can turn your everyday software engineering job into a thrilling journey? Our post, "The Top 10 Things You Can Do To Rock Out Your Software Engineering Job", has answers! Discover a treasure trove of strategies to amplify your workday. Don't miss out on these gems that can take your career to the next level! ➡️ [Link]
