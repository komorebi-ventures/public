I am grateful for cloudy days during a hot summer.

#givethanks

---

Watch this youtube video by Lenny Rachistsky and break it down for my followers. 
https://www.youtube.com/watch?v=dP8NmcEkxJI

<!-- NOTES:

Developer Productivity - How much we can get done now and over time. Software is a team sport (community effects). Individual wellbeing is important for the team to be its best. Well done creates improvments in well being, in sustainablility, and reductions in burnout.

Developer Experiance - Developers are our customers. Make it friction free. Reduce the uncertainty of the software development process.

DevOps - The cabailities, tools, and processes, to improve our software delivery end to end to improve speed and reliability of everything. 

Everyone would like to go faster, but not at the cost of reliability. 

DORA Framework - Leadtime to change, deployment frequency, mean time to restore, change failure rate. Speed and stablity move together. The faster you move the more stable you become. Smaller changes, less blast radious, more frequency provides better reliability outcomes and better quality outcomes. 

Both small or large companies improved outcomes if they are improving these metrics. 

Can not improve these through brut force. That will eventually cause you to burn your team out.

I need to read accelerate

These give you speed and stability in development and speed and stability help you earn the bussiness moeny. 
Automated Testing
CI/CD
Trunkbased Development
Version Control System
Good Technical Practices
Loosly Coupled Archtecture - Teams can change the system independently of one another. 
Cloud
Development Culture


SPACE Framework - A way to measure developer productivity or complex creative work. SPACE gives you a framework to find the right metrics for you. 

Satisfaction/Wellbeing
Performance - the outcome of a process
Activity - anytime you have a count or number of 
Commuinication and Collaberation - How people conenct
Effiecency and Flow - Flow through the system, flow 

DORA is an implementation of SPACE for the Outer Development Loop. 
For SPACE to work you need to pick metrics from at least 3 of the catigories to improve your desired outcomes.

Read the following if you want to learn more: 
SPACE Paper - ACMQ
Data from People and Data from System - Mc Carson
How to measure anything - Hubbard
Future Book - 

My favorite quote so far "How can I convince my executive team [DevEx] is important, because [my] developers are burning out, or [They] use computers in anger every day?" 

-- Nicole Forsgren

Steps to take if you want to do developer expeirance:
- Start where the biggest problem is
- See if there is any data related to the problem

Google does DevEx really well. They measture things systematically, and they continue to invest time in DevEx surveys. 

4 Box Framework

Good stratagy bad stratagy
By Richard Rumelt

Designing your life
By Bill Burnett and Dave Eveans

Enders Game
By Orson Scott Card

Ask some developers about their workflow, and tools, and ask them what the biggest barrier to their workload is. 

-->

This week I stumbled upon a great [episode](https://youtu.be/dP8NmcEkxJI) from one of my favorite podcasters Lenny Rachitsky. [Lenny's Podcast](https://www.youtube.com/@LennysPodcast) is generally focused on the product side of software development but often talks about larger issues that touch the more technical side of things. In this episode, he gets into a subject near and dear to my heart, developer experience (DevEx) with his guest Nicole Forsgren.  

Some highlights from the episode include a discussion about the differences between developer productivity, developer experience, and DevOps. 

The difference between the DORA and SPACE frameworks and the DevOps patterns and practices that have been shown to have the most impact on a business's success. 

The part of the episode I found most impactful was a reminder that no matter the size of your organization improving the 4 DORA metrics (lead time to change, change failure rate, change frequency, and mean time to restoration) improves business outcomes. In other words, your company is more likely to make money.

My absolute favorite part was Nicole's answer to: What's the most common question people who want to move into a DevOps journey ask you?

Her answer was priceless. "How can I convince my executive team [DevEx] is important, because [my] developers are burning out, or [They] use computers in anger every day?"

Do you perhaps have some developers using their computers in anger every day? If so give the [episode](https://youtu.be/dP8NmcEkxJI) a watch you'll be glad you did. 

---
1 

In Software Engineering the conversation is more about tradeoffs than right and wrong. 

For example, do I build out a monolith or a microservice architecture? 

A monolith is simpler to understand, build, and maintain.

Microservice architectures are harder to build, maintain, and understand but they allow for loose coupling of the full application and easily enable individual teams to work independently from one another.

If you are an early-stage startup you more than likely will have a better go of it building out a monolith

If you have a highly successful scale-up it may be worth considering microservices to improve developer experience and overall speed of delivery, but you will pay for it in added complexity.

It is all about the tradeoffs and which scenario is more liveable for your circumstances.

#SoftwareEngineering #Microservices #StartupTechChoices

2 

In the realm of Software Engineering, it's not so much about picking the "right" or "wrong" approach but rather about making informed decisions based on specific circumstances.

Consider the decision between monolithic and microservice architecture. A monolith is generally easier to grasp, build, and maintain. On the other hand, a microservice architecture, though more complex to develop, maintain, and understand, offers the benefit of loose coupling, enabling teams to work in an independent and flexible manner.

For those in the early stages of startup life, a monolith could be a more practical choice. However, for established and rapidly scaling companies, adopting a microservice architecture might prove advantageous to boost delivery speed and enhance the developer experience, despite the added complexity.

Ultimately, the choice hinges on the tradeoffs you're willing to make and which scenario best suits your unique situation.

#SoftwareEngineering #Microservices #StartupTechChoices

3

In the field of Software Engineering, the choice is seldom between "correct" and "incorrect". Instead, it's more about assessing tradeoffs.

Take for instance the choice between constructing a monolith and a microservice architecture. The former is simpler to build, understand, and maintain, while the latter, though demanding in its construction and upkeep, provides the benefit of loose coupling and promotes independent team operations.

For startups in their infancy, a monolith might serve better. But as the organization grows and scales, shifting towards a microservice structure could improve development efficiency and speed of delivery, albeit at the cost of added complexity.

Ultimately, it's all about weighing the tradeoffs and deciding which route is the most accommodating for your specific needs.

#SoftwareEngineering #Microservices #StartupTechChoices

4

In Software Engineering, the discourse often revolves around tradeoffs rather than unequivocal "right" or "wrong" paths.

For instance, when deciding whether to develop a monolithic architecture or a microservice one, it’s essential to weigh up their respective advantages and challenges. A monolith is typically simpler to construct and manage, while a microservice architecture, although harder to develop and maintain, allows for an easily decoupled application and encourages autonomous team collaboration.

If you're steering an early-stage startup, you'll likely find that a monolith serves you better. But for those leading a successful scale-up, embracing microservices might enhance the developer experience and hasten the delivery process, even if it increases complexity.

The crux of the matter? It's about considering the tradeoffs and determining which approach aligns best with your circumstances.

#SoftwareEngineering #Microservices #StartupTechChoices
---
The Essence of Strategy is chosing what not to do. 
-- Michael Porter

The key to good strategy is knowing what not to do. The key to executing on a good strategy is not doing it.

-- If you know please tell me in the comments. 

---

!false
 
(It's funny because it's true.)

---

