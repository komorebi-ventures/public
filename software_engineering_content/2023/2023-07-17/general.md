I am grateful to not be doing this alone.
#givethanks

---

This week break down this article by Hussein and share it with my followers. 
https://www.linkedin.com/feed/update/urn:li:activity:7004607875545272320/

Every specialty in software engineering can be deep and complex and backend engineering is no exception. This week I read [a great article](https://www.linkedin.com/feed/update/urn:li:activity:7004607875545272320/) by Hussein Nasser that covers some of that complexity at a high level. 

Some of the goodness that Hussein shares includes: 

- The significance of understanding the first principles.

- The necessity of grasping the underlying protocols.

- The importance of effective web server configuration.

- Understanding the critical attributes of databases and their unique applications.

- The role of proxies in modern backends.

- The use of messaging systems to minimize coupling.

- The impact of message format choice on client-backend communication.

- The essential nature of comprehending security risks and mitigation tactics.

- The continual need for learning and adaptability in an ever-evolving field.

If any of the ideas above or backend engineering in general is something that interests you give [How to Become a Good Backend Engineer (Fundamentals)](https://www.linkedin.com/feed/update/urn:li:activity:7004607875545272320/) a read, you won't regret it.

---
1 

You will do your best work in Software Engineering when you can enjoy large swaths of time in deep focus.

The best of us engineer their environment to maximize the time they can be in that state.

One way they do that is invest in a good pair of noise-canceling headphones. They are not cheap but being able to flip a switch and tame the chaos you can’t control is priceless. 

If you don’t have a pair I highly recommend you save up for one. 

If you already have a pair, what are they, and how do you like them?

#startupengineering #softwareengineering #workfromhome #deepwork #techtips 

2 

You will do your best work in Software Engineering when you can enjoy large swaths of time in deep focus.

The best of us engineer their environment to maximize the time they can be in that state.

One way they do that is by having a dedicated space where they work most of the time. 

It can be good for creativity to sometimes vary where you work but that dedicated space should only be used for work. That will make it easy for you to know when you are there you are in that focused zone. 

If you don’t have a dedicated space you work from day to day I highly recommend you find one.

If you do have a dedicated space, do you feel like it helps you get into a state of deep focus?

#startupengineering #softwareengineering #workfromhome #deepwork #techtips 

3 

You will do your best work in Software Engineering when you can enjoy large swaths of time in deep focus.

The best of us engineer their environment to maximize the time they can be in that state.

One way they do that is by putting time on their calendars for deeply focused work. 

This allows you to plan when you will actually be engineering. It also limits meeting creep from consuming your entire day.

If you don’t already add time to your calendar to actually do engineering I can’t recommend it enough. 

If you already follow this practice, how is it working out for you?

#startupengineering #softwareengineering #workfromhome #deepwork #techtips 


---

Common programmer thought pattern: there are only three numbers: 0, 1, and n.
-- Joel Spolsky

---
C programmers never die. They are just cast into void.
-- Alan Perlis.