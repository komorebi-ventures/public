# Transition to a Profit: Why Your Product’s Reliability Matters Most

Every successful software product has to solve real problems for real people. If you solve a real problem early adopters will be hell-bent on solving it with your product, regardless of the bugs, poor ui, or indeterminate behaviors. That is an exciting place to be but those early adopters won’t pay the bills. If you're going to make the transition to a product that generates a profit you need to ask yourself one question.

What is the most important feature I can add to drive revenue? 

The answer: RELIABILITY

Does that sound ridiculous? If it does it’s probably because most development organizations set themselves up as feature factories. You're measured by the number of lines of code you wrote, the number of PR’s you submitted, the number of deployments you made, not how happy your users are. Not how much revenue the product generates. Those concerns belong to someone else, not us. By optimizing for engineer productivity we often don’t have a second thought about if we are building the right things to help the business succeed. Left unchecked, that relentless drive will turn even amazing products into a kluge that most of the time doesn’t work. Software that solves a problem but can’t be trusted to be there when it's needed doesn’t get used, and people don’t pay for software they don’t use.

Once we have a product that is useful we have to start building reliability into it a bit everyday. How do you start to build reliability into your products? The first step is measuring how your users experience the  system. 

Attributes of your system that your users care about include:
- Availability: Is your system available when it's needed?
- Latency: How quickly does your system respond?
- Durability: Can a user retrieve the data they need when they want it?
- Consistency: Does the data your system returns match what they gave you?
- Correctness: Does the system return the right user data?
- Time to Restore: When the system stops working, how quick do you get it back online?
- Security: Is the data they store with you safe?
- Usability: Can the users figure out how to actually use your software?

Once you can measure these you can start having conversations with your clients to figure how much reliability is needed. Then you can engineer the system to meet those needs too.

What user metrics did I miss? Tell me about them in the comments!

#StartupEngineering #Profitability #BusinessStrategy



---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/measure-your-users-can-rely-software-robert-mccleary/). 

---
Whenever your ready, there are three ways I can help you: 

Consulting Services: Need help creating a reliability strategy? Feel free to reachout to me.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Interested in reading the books that have shaped my thinking? Buy my book list. 

Promote yourself to my 350 subscribers: Interested in sponsoring this newsletter? Reach out and let's discuss your needs.

---
Prompt: Create three teasers that encourage people to sign up for the LinkedIn Standup ahead of this article going live. 

Teaser 1
"What's the single most critical factor that takes a product from early adoption to profitability? Join us in our upcoming LinkedIn Standup to find out. Hint: It's not what you think! #ProductManagement #TechIndustry"

Teaser 2
"Ever wondered why some products succeed while others, with similar features, fail? The answer lies in one underestimated aspect. Sign up for our LinkedIn Standup to unravel the secret to transitioning to profitability. #SoftwareDevelopment #BusinessStrategy"

**Teaser 3** 

What if the key to building profitable software isn't about adding more “features”, but rather improving what's already there? 

Intrigued? 

Subscribe to LinkedIn Standup today so you don’t miss out on Saturdays article where we explore this concept and the impact it could have on your business. 

#ReliabilityEngineering #CustomerSatisfaction #SoftwareDevelopment #BusinessStrategy

---

Prompt: create 3 pieces of copy to be delivered via LinkedIn that will help drive traffic to this article on the LinkedIn Standup.

Engaging Question Post:
**1**
What's the most critical feature for your software product? 

If 'more features' was your answer, you might need to rethink! 

In this weeks LinkedIn Standup learn why RELIABILITY can be the game-changer. 

Don’t miss it! 

#ReliabilityEngineering #SoftwareDevelopment #BusinessStrategy


Insightful Statement Post:

In the race to develop more features, we often overlook the importance of RELIABILITY in software products. 

This can be a determining factor in your product's profitability. 

Learn more in our latest LinkedIn Standup article. Share your thoughts below!

https://www.linkedin.com/pulse/transition-profit-why-your-products-reliability-matters-mccleary

#CustomerSatisfaction #ReliabilityEngineering #BusinessGrowth

Call-to-Action Post:

Did you know that the reliability of your software product can directly impact its profitability? 

Is it time to shift your focus from quantity to quality. 

Join us in exploring this vital topic in our LinkedIn Standup article. 

Your software’s success might just depend on it! 

#SoftwareReliability #BusinessStrategy #Profitability

https://www.linkedin.com/pulse/transition-profit-why-your-products-reliability-matters-mccleary
