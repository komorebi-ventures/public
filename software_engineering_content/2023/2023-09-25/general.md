[Posted 9/26/23 10:05AM MT](https://www.linkedin.com/feed/update/urn:li:activity:7112467004900470784/)
[Scheduled for 9/26/23 8PM MT](https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7112616846402482176-UrP7?utm_source=share&utm_medium=member_desktop)

Do you ever feel like literally nothing is going the way you want or hoped for? Like all the most important things in your life have turned on you and now you're surrounded with no way out?

I do, I feel that way right now.  Not very long ago I would have let it really tear me apart. I would have allowed my mind to fill with judgment about my worth as a man and all manner of insults I would never say to even my most ardent adversary. 

Not today though. Today I am going to do what I can, with the resources I have available and that is going to be enough. 

I am going to take a note out of the mind of Anthony McAuliffe: 

“Men, we are surrounded by the enemy. We have the greatest opportunity ever presented an army. We can attack in any direction.”
Instead of seeing the trials of the day as a reason to surrender, I hope today I can look up at what feels like being completely surrounded on every side and smile as I say “This is the greatest opportunity I have ever been presented with…I can move forward in any direction”

In fact I hope I can take that attitude with me one day at a time even after my fortunes turn again in my favor, because they will.

I am grateful for this unprecedented opportunity. 

What about you? Are you surrounded by the harsh realities life provides? Are you going to move forward despite the difficulties? Write In the comments what your doing today to take advantage of “the greatest opportunity” you have ever had.

#givethanks

---
[Posted on 9/27/23](https://www.linkedin.com/posts/robertmccleary_strategy-softwaredevelopment-startups-activity-7112867513591353345-RE7x?utm_source=share&utm_medium=member_desktop)
Build a post around this post https://www.linkedin.com/posts/mvneerven_strategy-softwaredevelopment-startups-activity-7101862306342424576-bmH9?utm_source=share&utm_medium=member_desktop 
and this article https://medium.com/cto-as-a-service/5-things-founders-investors-and-recruiters-should-know-about-the-cto-role-a65d7bb66264 from Marc van Neerven.

Do you know Marc van Neerven? He's a fractional CTO who consistently shares insightful tech content here on LinkedIn.


Today, I came across a thought-provoking post by Marc that has me thinking about the balance between strategy and operations in tech startups. Marc's key point is that there's an inherent conflict between these two perspectives. Being excellent at both can be challenging because they require different mental models. In fact, Marc seems to suggest that the human brain can't effectively maintain both models simultaneously.

Marc emphasizes that a CTO plays the role of the chief technical strategist. Consequently, they can't be fully immersed in the day-to-day operations of the R&D team or production issues and still maintain the necessary forward-looking, cross-functional connections required to ensure technology aligns with long-term business needs.

My experience seems to jive with this perspective. In my interactions with multiple startup CTOs, those who focused primarily on operations often struggled to align with the broader business vision and long-term goals. Conversely, CTOs with a strong strategic focus often found it challenging to keep the  operational details straight.

What are your thoughts on this idea? Can a Startup CTO genuinely avoid hands-on coding and still maintain a clear and forward-moving technical vision? Do you know of any books or articles that delve deeper into this concept?

If this topic piques your interest, I encourage you to check out Marc's post and article below. They're both excellent reads that delve further into this intriguing topic.

https://www.linkedin.com/posts/mvneerven_strategy-softwaredevelopment-startups-activity-7101862306342424576-bmH9?utm_source=share&utm_medium=member_desktop

https://medium.com/cto-as-a-service/5-things-founders-investors-and-recruiters-should-know-about-the-cto-role-a65d7bb66264

---

Computer science education cannot make anybody an expert programmer any more than studying brushes and pigment can make somebody an expert painter.
-- by Eric S. Raymond.


---
[posted on 9/29/23 @ 8AM]()
[reposted on 9/29/23 @8PM]()

Why did the dev walk out of the NoSQL bar?...because it had no tables.
#fridayfunnies

[previous version](https://www.linkedin.com/posts/robertmccleary_why-did-the-dev-walk-out-of-the-nosql-bar-activity-6983769060580753408-s7zS?utm_source=share&utm_medium=member_desktop)
