This was [posted](https://www.linkedin.com/feed/update/urn:li:activity:7112481300426489857/) on 9/26/23 @ 11AM MT - 349 impressions first week 

[Reposted on 9/26/23 @ 8PM MT](https://www.linkedin.com/posts/robertmccleary_discover-the-power-of-devops-transforming-activity-7112616844066263040-r9ro?utm_source=share&utm_medium=member_desktop) 243 impressions first week
[Seceduled for 10/23/23 @ 9AM MT]()
[Scheduled for 11/20/23 @ 10AM MT]()

# Demystifying DevOps: Uniting Teams with Continuous Improvement

Do you ever get confused about what DevOps is and isn’t? Over the years there have been a ton of differing opinions and if you listen to the vendors you will only ever get DevOps if you buy their tools.

DevOps came out of the agile philosophy of continuous improvement. We realized more than a decade ago that having the people who write the software and those who operate the software at war with each other was a horrible idea. In a DevOps culture we unite the once warring factions with a set of shared metrics and a philosophy of continuous improvement to help the entire organization to move forward together. 

In DevOps, we review where we are, decide where we want to go, try something to get closer to the goal, measure our results, and repeat the process. 
It's an abstract and complex concept, which makes it hard to understand if this isn't something you have lived through before, so let me help illustrate the point with a story.

## Story Time
You are the CTO for the well established bootstrapped startup MicroCo. Your glorious technology division has a full complement of eight people, including yourself. Your product has a sound footing in your target market, and your roadmap is full of excellent additions that will delight your current and future customers. 

As MicroCo has become successful, highly determined venture capital fueled competitors have entered the market and are developing their features at a pace you can't match.

You and the MicroCo CEO realize your team needs to deliver your roadmap on an accelerated timeline to stay in the game. To develop more quickly, you could hire, but the business fundamentals can't currently support that, and taking venture funding comes with too many tradeoffs you don’t want to make. You could impose unreasonable output expectations on your team, but that can only improve the delivery rate for a short time and risks decreased quality, developer burnout, and attrition. 

The next place you look is at your processes. You find that your current build, deploy, and test process requires about 85 hours of developer time each week. You figure there has got to be a way to reduce that time, so you have two of your developers' string together each step via automation. In other words, you implement CI/CD. When the project is complete, you have reduced the time your developers spend on this process by 64 hours which increases your delivery velocity substantially.

With the data in hand, you now get back to the CEO and decide if the increased velocity is enough for your needs. If it is not, you look for another place to improve things to increase the speed of roadmap delivery. If it is, you decide what else can be improved to deliver value to your clients.
In this story, my friend, you just lived the DevOps dream! Not by getting developers to create a CI/CD pipeline but by completing a continuous improvement cycle.

The nature of existence is changing. To survive, we must be adaptable, and continuous improvement provides a framework to evolve ourselves, our processes, our tools, and our business. If you're not routinely taking advantage of continuous improvement cycles in your organization you're missing out on powerful opportunities to create the best team and company you can.

So what do you think? Do you use continuous improvement regularly? What's the most impactful change you and your teams have made by following these types of feedback loops?

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/continuous-improvement-foundation-devops-culture-robert-mccleary). 

---
Whenever you're ready, there are three ways I can help you:

Let's Collaborate: I'm on the lookout for exceptional companies to partner with in various capacities, whether that's full-time employment, part-time advisory roles, or one-time consulting services. If you're interested, schedule a [brief call](https://topmate.io/robert_mccleary/450909) with me today.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Gain insight from the books that have shaped my thinking. Buy [The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi) today! 

Maximize your brand visibility by sponsoring this newsletter. Connect with an engaged audience in the tech industry. [Set up a time today](https://topmate.io/robert_mccleary/450948) to discuss the opportunity.

---
Copy 1:

Discover the Power of DevOps: Transforming Chaos into Collaboration

Uncover the essence of DevOps, break free from vendor-driven confusion, and embrace the philosophy that's reshaping organizations. Dive into our latest article to explore how DevOps bridges the gap between software creators and operators, propelling your team forward. Join us on this journey of continuous improvement.

Read Article

Copy 2:

Revolutionize Your Approach: The DevOps Guide You've Been Waiting For

Are you puzzled by DevOps? Don't be! Our article demystifies this complex concept with real-world insights. Learn how a CTO at a thriving startup used DevOps to outpace competitors without hiring or sacrificing quality. Explore the transformative power of continuous improvement—read now!

Unlock the Secrets

Copy 3:

DevOps Unleashed: A Tale of Acceleration and Adaptability

Witness the DevOps dream in action! Follow a CTO's journey as they conquer challenges, boost delivery speed, and stay ahead of rivals. Dive into our article and discover why continuous improvement is the key to survival in a changing world. Are you seizing the opportunities for growth?

Start Reading