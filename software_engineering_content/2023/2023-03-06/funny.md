The most disastrous thing that you can ever learn is your first programming language

-- Alan Kay

---
From: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66 