[Schduled for 10/03/23 @8AM](https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7114972366123765761-1Ihv?utm_source=share&utm_medium=member_desktop)
Impressions:419

[Scheduled for 10/3/23 @8PM](https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7115153557691015168-9DHv?utm_source=share&utm_medium=member_desktop)

Impressions: 543

I am grateful for the people who smile at me during my morning walks.


#givethanks

---

[Give this a read and post about it](https://www.linkedin.com/posts/nireyal_motivation-technology-personaldevelopment-activity-7112405507390205952-ZRVB?utm_source=share&utm_medium=member_desktop)

[Schduled for 10/04/23 @8AM](https://www.linkedin.com/posts/robertmccleary_motivation-technology-personaldevelopment-activity-7115334763510472704-OZ65?utm_source=share&utm_medium=member_desktop) 

Impressions:215 

Today, I stumbled upon a fascinating post by Nir Eyal that delves into the intriguing concept of peak creativity spawning out of boredom. Eyal highlights some compelling research suggesting that our most innovative ideas often emerge during seemingly idle moments, such as in the shower or when we're about to drift off to sleep. According to him, our minds reach peak creativity when they're unoccupied.


Eyal even goes as far as recommending a deliberate daily practice of embracing boredom, aiming to harness this creative phenomenon without interrupting our sleep.


I've decided to put this idea to the test myself. This week, I'll allocate 15 minutes each day to intentionally experience boredom and see what happens.


What do you think about this? Does it resonate with you? Or do you have alternative methods for unlocking your creativity?


If I've piqued your interest, check out the article linked in Eyal's post; it promises an enjoyable read.

---
[Scheduled for 10/05/23 @ 8AM](https://www.linkedin.com/posts/robertmccleary_first-make-the-change-easy-warning-this-activity-7115697143574626304-DB2D?utm_source=share&utm_medium=member_desktop) 

Impressions: 320

[Scheduled for 10/05/23 @ 8PM](https://www.linkedin.com/posts/robertmccleary_first-make-the-change-easy-warning-this-activity-7115878343719231488-a1AV?utm_source=share&utm_medium=member_desktop)

Impressions:116

I found this one here: https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr 

First make the change easy (warning: this might be hard), then make the easy change.
-- Kent Beck

---

[Scheduled for 10/06/23 @ 8AM](https://www.linkedin.com/posts/robertmccleary_fridayfunnies-activity-7116059554018181120-QouQ?utm_source=share&utm_medium=member_desktop) 

Impressions: 4870

[Scheduled for 10/06/23 @ 8PM](https://www.linkedin.com/posts/robertmccleary_fridayfunnies-activity-7116240720306348032-ByDI?utm_source=share&utm_medium=member_desktop)

Impressions: 410

This came from: https://www.linkedin.com/posts/robertmccleary_why-did-the-programmer-quit-his-job-because-activity-7021830758927409152-9EBC?utm_source=share&utm_medium=member_desktop

Why did the programmer quit his job?

Because he didn’t get arrays.

#fridayfunnies