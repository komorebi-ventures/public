This was [scheduled for 10/2/23 @ 8AM](https://www.linkedin.com/posts/robertmccleary_discover-the-unvarnished-truth-about-cloud-activity-7114610008520065026-udwU?utm_source=share&utm_medium=member_desktop)
[Reposted on 10/2/23 @ 8PM MT]()
[Scheduled for 10/30/23 @ 9AM MT]()
[Scheduled for 11/27/23 @ 10AM MT]()

---
# Don't Buy the Hype: The Cloud is Just a Tool, Not Your Friend

I came of age in a cloud world. AWS, GCP, and Azure all predated my current career, and the allure they provide is strong.
They promised If you come to the cloud:
- You won't have to hassle getting IT approval or wait for them to build and deliver the necessary infrastructure.
- You can easily scale your hot new project to meet any demand profile.
- Engineers won't need to waste time learning how to operate your hardware, you can hire fewer of them, and spend more time on delivering direct business value.
- You will save money because they operate with economies of scale you can't imagine.

I bought the marketing hook, line, and sinker. I parroted the new party line whenever it was appropriate and many times even when it wasn't.

It turns out:
- Even if you bypass IT, someone is responsible for approving the costs, pushing the buttons for the cloud to build it and delivering access to the infrastructure.
- The You Ain't Going to Need It (YAGNI) principle applies. The vast majority of new projects will never reach internet scale.
- The team size needed to maintain a sprawling cloud-based infrastructure is just as large, if not larger, than what it took to maintain your own server farms.
- No one saves money long-term in the cloud. The cost reduction many companies experienced during their move was a result of choosing what should live in our brave new world and what should die. In other words, it was a function of ruthless prioritization, not an inherent benefit of the cloud. 

## A Case in Point:
Over the last year 37signals decided to do what has become anathema and heresy in most technology circles by leaving AWS to host their SaaS products on their own hardware in a colo. If you follow David Heinamire-Hanson and Jason Friend much you know they get immense satisfaction from proving that industry standards are meant to be broken. That fact notwithstanding their rationale for leaving the cloud was refreshingly simple, they realized they could keep more of their companies hard earned money by making the switch. According to an [post](https://www.linkedin.com/posts/david-heinemeier-hansson-374b18221_our-cloud-exit-has-already-yielded-1myear-activity-7108557024728084482-PWfp?utm_source=share&utm_medium=member_desktop) by DHH they are already saving $100k/month, and when their spend commitments to AWS are over they will be spending nearly $2M/year less in their new situation. I can't speak for every organization on the planet but there really are times when having that kind of cash on hand is worth doing things a little differently.


## TL;DR
Cloud computing is a tool, not your friend. Don't get caught up in the marketing hype. Don’t assume it's the only right way. Like any decision, learn the tradeoffs, then make the decision knowing what you will give up for the benefits.

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/cloud-computing-tool-your-friend-robert-mccleary). 

---
Whenever you're ready, there are three ways I can help you:

Let's Collaborate: I'm on the lookout for exceptional companies to partner with in various capacities, whether that's full-time employment, part-time advisory roles, or one-time consulting services. If you're interested, schedule a [brief call](https://topmate.io/robert_mccleary/450909) with me today.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Gain insight from the books that have shaped my thinking. Buy [The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi) today! 

Maximize your brand visibility by sponsoring this newsletter. Connect with an engaged audience in the tech industry. [Set up a time today](https://topmate.io/robert_mccleary/450948) to discuss the opportunity.

---
## Copy

🤯 Prepare to Be Amazed! Our Upcoming Article Will Change the Way You Think About the Cloud. Subscribe to the LiknedIn Standup Today to Have it Delivered to Your Inbox when it Drops! 🎩

Discover the Unvarnished Truth About Cloud Computing. Don't Buy the Hype! Read our latest article to uncover the realities and trade-offs behind the cloud technology buzz. 👉 

Are You Overselling the Cloud? Our Article Reveals the Costly Myths and Realities. Dive into the cloud's true nature here. 👓 

The Cloud: Friend or Foe? Learn why it's more than just a tool in our eye-opening article. Get the facts now! 🌥️ 
