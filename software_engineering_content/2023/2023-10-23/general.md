I am grateful for people who give without reservation to make things better for everyone.


#givethanks

---

The best performance improvement is the transition from the nonworking state to the working state.

-- J. Osterhout

from: https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr 

---

//be nice to the CPU
Thread_sleep(1);

#fridayfunnies

from: https://www.one-beyond.com/20-one-liners-only-software-developers-understand/