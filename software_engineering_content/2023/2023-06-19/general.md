I am grateful for the people who look out for me.

#givethanks

---

As a software engineer do you ever get the feeling that there may be too much to manage?

You need to think through, the product and its features, how the code and the system are architected, the languages, the frameworks, security, reliability, performance, cost efficiency, data durability, data correctness, quality, user experience, and the list goes on.

You need to think through all of that while managing the constant pressure from management to push through this feature and then move on to the next one. 

So what do you do? You start by building something useful to the end users that they will pay to use. Until there is money flowing in because you built something worthwhile none of the rest of it matters. Once the money is flowing in then you have time and a way to influence the rest of your company to invest in building out the rest well. 

Don't over-engineer. Do what it takes to create revenue, and let the revenue push the full build out forward.

---

TThe #1 rule of Code Reviews? Don't be a jerk. 

The purpose of code reviews is to gain a second viewpoint which helps detect if something isn't suited for the purpose or if there will be a second-order effect by rolling things out.

Be candid, be humble don't be a jerk. 

---

What's the difference between junior and senior software engineers?

A senior software engineer writes the wrong code faster.

#fridayfunnies

Adapted from: https://upjoke.com/software-jokes
