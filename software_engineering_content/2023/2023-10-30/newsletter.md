# tl;dr
- We should be open and honest about the tradeoffs we are making with our choices at every level: personal, business, governmental. 
- Giving people (and the companies they build) as much choice as possible increases the opportunities for novel outcomes.

Do you ever see a problem in the world and know if stupid people would just do what you say everything would be better? Humans share a desire to improve ourselves, our environments, and our society. Unfortunately we often ignore the full ramifications of our choices in our pursuit of “better” outcomes. A good example is the legislative trend in the US that requires companies to disclose the salary range for positions they are publicly hiring for.

# Story Time
It’s mid July in 2011, and I am pacing in the living room of my apartment. My wife and two children are watching me as I anxiously anticipate what the man on the phone would say next."We are impressed with your education and love your enthusiasm for GIS software development. I want to offer you the opportunity to join our team," he said in a clear, confident voice. 

I looked over at my wife with what had to be the stupidest-looking ear-to-ear grin she had ever seen. 

I had graduated at the end of April, which ended my employment in a research lab. It had been months since I earned a dime. We were living on a swiftly diminishing supply of money. I felt desperate to land anything, and my hopes were high that this would be the end of my search.

"We would like to offer you the title of Software Engineer with a base salary of $32k per year". It took a minute for that to sink in. You see, $32k per year was about $10k a year more than was offered for the fellowship to grad school I had turned down. I was in shock because it was so low. I knew people who were getting offers at other companies 2 - 3x that amount. I didn't know what else to say but "Thank you. I am going to need to think about it," With that, we ended the conversation, and I sunk onto the couch next to my wife. Could we live on that in Anchorage, Alaska? I couldn't imagine that we could, and a quick internet search backed up that thought. At that salary, my family of four would be well below the poverty line. 

What was worse, this was the exact job I wanted. It was software development for a geographic information systems company. The company had a great culture, and two of my friends from University had already started working with them and loved it.

The turmoil I felt was intense. It was the job I dreamed of, in the field of my choice, but at a salary so low that my family would be in even worse shape than they were during my time at University which by all accounts was terrible. My frustration with the process was palpable. I had spent at least a dozen hours applying, interviewing, and testing for this position. Why didn't they tell me that the salary range was so low? 

# Mandatory Salary Disclosure
This isn't a unique experience. Virtually everyone who has ever tried to get into a professional position after University has experienced it. 

In fact there is a trend spreading across the US to change that. Many states and local jurisdictions are passing legislation that require companies to disclose the salary range of jobs they advertise. On LinkedIn, I see a ton of hype around this, with a strong focus on how great it is that these “bad companies” will have to start doing things right.  

Just because a company doesn’t disclose salary ranges doesn’t make them bad. The company in my story wasn't evil, and they weren't giving me a hard time. They filled a low profit niche, and if they had to compete on salary alone, they wouldn't be able to hire people like me, or my friends. My experience was frustrating but merely annoying in the big picture. 

From my perspective legislating a solution to this problem is like driving a thumbtack into a corkboard with a sledgehammer. Does it do the job? Probably. Are there unintended consequences? Absolutely. Every personal, business, or society choice we make has tradeoffs. 
Let's consider the possible tradeoffs of mandatory salary disclosure.If we force every company to disclose salary we end up with less wasted time, that sounds great. Some of the tradeoffs may be:

- Professionals will filter out companies not offering their ideal salaries before considering other critical aspects like company culture, exciting projects, professional growth, ect.
- Salary ranges will further homogenize between companies.
Lower-performing professionals will be priced out of the market.
- Top-of-market salaries will be lowered, reducing lifetime earnings for top performers. 
- More people will expect to be paid at the top of band even if their abilities can’t justify the higher wages
- Some firms will go out of business because they can't support the new market rates.
- There is a cost to the companies for maintaining salary data for each position and proving they are complying with the law.
- If you know where a person works and their job title you now have information about how much they earn leading to a decrease in personal privacy.

In short, we are making the market far less efficient by increasing focus on one small aspect of what a job is. At best, the overall outcome is a wash; at worst, it is catastrophic.

So should we require all firms to advertise the salary range of their jobs? I really don't know, but I would guess not. 

This might sound heretical to you but I think we should be giving people as much freedom as possible to do what they believe is right. That leads to diverse choices that increase the variety of outcomes. 

# Deeper Conversations
Regardless of if we should make this the norm through legislation, we absolutely should be discussing the benefits and the tradeoffs of this and every policy we set for our companies.

What would have happened if this kind of legislation had been there back in 2011? There is a good chance my friends who joined the company would never have applied. That would have been a shame because they both had fantastic experiences and stayed for many years.

Back in my living room in 2011, I called the hiring manager, and I told him, "I just can't see how I can make that amount work." and we ended the conversation amicably. Even though it was heartbreaking, walking away was the right call for my situation, and not disclosing it ahead of time was likely the right call for their situation. 

# Conclusion
In conclusion, the tale of my job offer in 2011 and the ongoing debate over mandatory salary disclosure serve as reminders of the complexity of choices and the importance of weighing tradeoffs. Whether it's personal decisions, business practices, or governmental policies, we should engage in deeper conversations about the potential consequences of our choices. As we navigate the path to 'better' outcomes, we must recognize that sometimes embracing challenges and granting individuals and companies the freedom to do what they believe is right can lead to diverse choices and a broader spectrum of outcomes. Regardless of whether we legislate certain norms or not, it's crucial to discuss both the benefits and tradeoffs, ensuring a well-informed and balanced approach to our decisions

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/did-you-order-sledgehammer-robert-mccleary). 

---
Whenever you're ready, there are three ways I can help you:

Lets talk: Do you have questions, or want to share your perspective on this or any other topic? Go ahead and comment below or shoot me an email at robertjmcclearyii@gmail.com . I would love to have a conversation with you.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Gain insight from the books that have shaped my thinking. Buy [The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi) today! 

Maximize your brand visibility by sponsoring this newsletter. Connect with an engaged audience in the tech industry. [Set up a time today](https://topmate.io/robert_mccleary/450948) to discuss the opportunity.

---

# Teasers

I didn't do one of these because the newsletter was late this week.

---

# Copy
"Ever wondered about the true impact of mandatory salary disclosure on job seekers and companies? My personal story from 2011 sheds light on the complexities of this issue. Explore the tradeoffs and join the conversation in my latest newsletter. Read more: [Link] #SalaryDisclosure #CareerChoices #Tradeoffs"

"Are we making the job market less efficient by focusing on just one aspect of a job? My personal experience highlights the consequences of mandatory salary disclosure. Dive into the debate and share your thoughts in my latest newsletter. Check it out: [Link] #JobMarket #CareerDecisions #Newsletter"

"In a world where we all seek 'better' outcomes, we often overlook the full ramifications of our choices. My journey from 2011 reflects the challenges and tradeoffs of job offers and mandatory salary disclosure. Join the discussion in my newsletter. Read now: [Link] #Tradeoffs #CareerChoices #LinkedInArticle"