I am grateful for the hard days, they remind me how great the good days really are.
#givethanks

---

Simple things should be simple, complex things should be possible.

-- Alan Kay

From: https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr 

---

Why do programmers hate to work at night?... Because that's when the bugs come out.

Adapted from: https://www.quora.com/What-are-some-good-jokes-related-to-programming-and-or-software-engineering 

---