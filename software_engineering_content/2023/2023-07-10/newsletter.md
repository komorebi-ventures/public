# On CI/CD and that Other CD


SRE and DevOps share a few principles. One strong commonality is the belief that there is toil in almost every aspect of technology work. Work that is predictable, repeatable, and quite frankly boring. 


When faced with toil, the best engineers don't tolerate it too long. They actively look for ways to automate these tasks, and by doing so, they end up methodically increasing their velocity while keeping work interesting. 


One of the most laborious activities in software engineering is building, testing, and deploying software. DevOps has named the automation of this tedium CI/CD. 


## What is CI?


CI stands for continuous integration and is the automated process of ensuring new code checked into a repository meets certain quality expectations before being integrated (made into a new addition to your code base). The best CI has a few high-level steps that eliminate toil while improving the likelihood that a given change will be a net benefit to the team. 
Code linting 
Checks for known antipatterns
Automatically applies code style guidelines to improve readability
Vulnerabilities scanning (Static application security testing aka SAST)
Build the code
Run the unit tests
When CI finishes, you should be confident that the code built meets your quality standards, is optimized for readability, is free from obvious security problems, and is deployable.  


## What is CD?


CD stands for continuous delivery. Proper CD has a few steps that improve our confidence that a given deployment will not cause significant issues once released to production.
Builds are packages and made ready for repeatable delivery to arbitrary environments.
Packages are deployed to a testing or staging environment.
Application penetration testing (dynamic application security testing, aka DAST).
End-to-end testing is run.
Standard monitoring reports if the system is healthy.


## What’s the Other CD?


Things become more interesting when CD changes from continuous delivery to continuous deployment. Once checked in, the automation handles every detail needed to deploy the code to production. Continuous deployment systems have some essential features, including:
It deploys automatically when the rest of the pipeline passes.
It uses standard production monitoring to evaluate whether a given deployment is healthy and, if not, rolls the changes back.
The automation can be bypassed, so manual deployments or rollbacks can occur.
It can be disabled when needed.
At my current company, we have gotten the first step in continuous deployment built and it's an awesome addition to our stack.  I can’t wait to be able to build out the next layers and take advantage of their benefits. 


Adopting and maintaining a robust CI/CD system is a key piece of automation that eliminates major toil. Freeing your engineers from time consuming and repetitive tasks gives them an opportunity to deliver more value to your company and gives them more enjoyment in their work. At my current company (Filevine Inc.), we have gotten the first step in continuous deployment built and it's an awesome addition to our stack.  I can’t wait to be able to build out the next layers and take advantage of their benefits too. If you're grappling with the 'toil' in your build and deployment processes, I'd encourage you to consider similar strategies. 

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/cicd-other-cd-robert-mccleary). 

---
Whenever your ready, there are three ways I can help you: 

Consulting Services: Need help evaluating CI/CD or planning your own implementation? Feel free to reachout to me. 

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Interested in reading the books that have shaped my thinking? Buy my book list. 

Promote yourself to my 350 subscribers: Interested in sponsoring this newsletter? Reach out and let's discuss your needs.

---
**1**

Tired of the toil of building code?

Find out how CI/CD and that 'other' CD can revolutionize your software development process in this LinkedIn Standup article.

#DevOps #SRE #Automation

**2**

"Ready to transform one of the most laborious activities in software engineering into a breeze? Discover the power of CI/CD and the magic of the 'other' CD in my new newsletter. Read it now ➡️ [Link] #TechTalk #DevOps #CICD"

**3**

Ever wondered what the best engineers do to escape the boring aspects of tech work? 

They automate! 

Learn how to eliminate toil with CI/CD in my newsletter. 

Dive in here ➡️ https://www.linkedin.com/pulse/cicd-other-cd-robert-mccleary-1c/

#TechTips #SoftwareEngineering #Automation


---

"Dive deep into CI/CD and discover the 'other' CD that's changing the game in my upcomming newsletter. Don't miss out, subscribe today to receive it straight in your inbox as soon as it drops! 🔒🔧 #DevOps #SRE #TechInsights"

"In the next LinkedIn Standup we discuss how CI/CD and the 'other' CD can boost your engineering velocity. Sign up now and be the first to explore these powerful automation strategies right in your inbox! 🚀📨 #TechTalk #Automation #EarlyAccess"

"Ready to change how you see software engineering? Our upcoming newsletter explores how the best engineers combat the boring tasks. Learn more about CI/CD and the intriguing 'other' CD. Subscribe today to get it fresh off the press! 📬💡 #SoftwareEngineering #Automation #SignUp"

