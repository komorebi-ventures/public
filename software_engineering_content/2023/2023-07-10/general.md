I am grateful for the ability to learn from people all around the world because of the internet. 
#givethanks

---
In software engineering it's important to only rely on reliable things.

Need a crypto function?

Your options might be:
- Write one yourself
- Use that sweet  new library that just went into Alpha
- Use one backed by Google that is actively maintained and has been in use for over a decade.

Which do you choose?

Hint: The last one is probably best choice.

Variation 1:

When it comes to data management in software development, sticking to reliable solutions is a must.

What would you do for a data storage solution?

- Design your own database from scratch
- Test out that cool new NoSQL solution that's still in Beta phase
- Opt for a robust SQL database like PostgreSQL, that is backed by a massive community and has a proven track record of over 20 years

Where would you place your trust?

Hint: The last one is likely to be your best bet.

Variation 2:

In web development, a dependable foundation is key.

When selecting a front-end library, would you:

- Create your own JavaScript library
- Try that shiny new library that just went into pre-release
- Use something like React, backed by Facebook, constantly updated, and battle-tested for almost a decade

Which one sounds the most trustworthy?

Hint: The last one is the sensible choice.

Variation 3:

When building robust applications, reliability should be your top priority.

Need a cloud service provider?

Would you:

- Build your own data center
- Experiment with that trendy cloud service that was launched last month
- Rely on Amazon Web Services (AWS), a stable platform with a comprehensive suite of services, backed by Amazon, and in the game for over 15 years

Which seems the most reliable?

Hint: The last one would be a wise pick.


---
Lots of companies are focused on growth. In fact many of them state their purpose as obtaining permanent exponential growth. 

That is a mistake. In human experience nothing good grows forever exponentially. Chasing that goal often leads to soulless corporations, with unhealthy employees, degrading the environment, who harm their customers. 

In nature growth can be exponential at times but never indefinitely. We can learn alot from modeling systems after nature. In nature organisms only grow as they can gain resources to do so. Organisms have a certain amount of growth beyond which they become unhealthy. Additionally the rate an organism grows has a profound influence on longevity with the slower growing organisms having the longest lives.

We can build businesses modeled on natural organic growth. We can limit growth to prevent overgrowth and its accompanying maladies. We can focus on slow intentional growth to maximize the businesses longevity. We can make sure we only grow as we gain the resources to do so to prevent waste. Following this pattern improves the chances of the business providing serious value to its customers, employees and owners for the long term.

1. The chase for unending growth has become the obsession of many enterprises, aiming for a permanent state of exponential expansion. But is this truly beneficial? In the vast tapestry of human experience, limitless growth has rarely proven to be a positive endeavor. It often gives birth to heartless conglomerates, fostering stressful environments, damaging the Earth, and causing harm to those they're supposed to serve.

Nature teaches us a different lesson about growth: It can be rapid, but it never persists eternally. An organism's growth is dependent on the resources it can gather, and any growth beyond its natural limits only leads to decline. Similarly, the rate of growth impacts an organism's lifespan, with slower growth often correlating with longevity.

We need to take a cue from nature when we shape our businesses. By practicing organic growth, we can manage expansion, keeping it within healthy limits. By advocating for slow, intentional growth, we can aim to ensure the longevity of our enterprises. And by growing only as resources become available, we can curb waste. Following these principles, businesses are more likely to deliver long-term, meaningful value to stakeholders, employees, and owners.

2. Businesses are obsessed with the notion of endless growth, aiming for exponential expansion that they hope will be eternal. But is this a feasible goal? From what we've learned throughout human history, nothing good lasts forever—especially if it's growing without any restraints. This relentless pursuit often births corporate monoliths, with stressed employees, environmental degradation, and customer harm as byproducts. 

The natural world, however, tells a different story. Growth can be fast but it's never perpetual. Organisms grow according to the resources they can obtain and there's always a limit beyond which health starts to decline. Furthermore, the pace of growth has a considerable influence on an organism's lifespan—the slower the growth, the longer the life.

This wisdom can guide the way we build our businesses. By focusing on organic growth, we can avoid the pitfalls of over-expansion. Emphasizing slow, deliberate growth can enhance the business's lifespan. Ensuring that growth aligns with resource acquisition can help avoid wastage. If businesses heed these principles, they are more likely to deliver enduring value to customers, employees, and owners alike.

3. Many businesses are tunnel-visioned on the goal of perpetual growth. They strive for exponential expansion in an attempt to engrave their presence indefinitely. However, history shows that unchecked, ceaseless growth rarely yields anything beneficial. It can lead to the creation of soulless corporations, where stressed employees work in an environment that's harmful to them and their customers, while also causing ecological damage. 

In contrast, nature shows us that growth can be fast but it's never without limit. Organisms grow according to their ability to gather resources and there's always a threshold beyond which health starts to deteriorate. Also, the speed of growth often influences an organism's lifespan—slower growth generally results in longer life.

This organic model can be applied to how we develop businesses. By managing growth to keep it within sustainable boundaries, we can prevent problems associated with unchecked expansion. Prioritizing slow, measured growth can help ensure the longevity of the business. Growing only as resources permit helps avoid wastage. Adhering to these principles improves the likelihood of businesses delivering lasting value to their customers, employees, and owners.


---
Data is a precious thing and will last longer than the systems themselves.
--Tim Berners-Lee.

Taken from:
https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66

---

Debugging is like being the detective in a crime movie where you're also the criminal and the victim.

#fridayfunnies

Adapted from:
https://www.altcademy.com/blog/the-best-jokes-about-programming-and-software-engineering/ 

---