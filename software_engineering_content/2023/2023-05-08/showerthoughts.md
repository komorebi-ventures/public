The secret of the modern economy is that the actual money in circulation is limited. Our entire system relies on people being willing to exchange the money they have for the goods and services others create. 

No creation == a broken economy. 
No buying == a broken economy. 

The biggest hurdle our economy can face is our fear.

Fear to try to make new things because no one will buy them and fear to spend what we have on what we need an want. 

We should have enough resources reserved for an emergency, but curtailing creating and buying today out of fear for what the economy might look like tomorrow will create the very economy we fear. 

Its a self fulfilling prophecy. 

#showerthoughts.
---