Even the best planning is not so omniscient as to get it right the first time.
-- Fred Brooks

---
Taken from: https://www.comp.nus.edu.sg/~damithch/pages/SE-quotes.htm

