I have never been in a genuinely meaningless job. I have been given meaningless work. You can tell it's pointless because it doesn't change anything for anyone when it is finished.

I do think that there is likely an incentive for large companies to hire people and keep them working meaningless jobs. If the company has capital and nothing to use it on, hiring and retaining people is a net benefit because it deprives would-be rivals of the means to challenge them successfully. It is brilliant in a dumb way.

I love startups because of the impact I can have doing real and meaningful work. I still get the occasional meaningless assignment, but it is so rare compared to my stint with giant multinationals that it's worth every tradeoff.

I want to make a difference, and I will always gravitate to opportunities that give me that chance over those that waste my time.

I believe that we can rebel against the meaningless, and not just by working at startups. We can push back on our bosses and HR when they give us things to do that don't improve anything. We can take the initiative and find meaningful ways to contribute to the business, and we can love every minute of it. I don't think we will always win the fight, but some things are worth the risk of a loss, and this is one of them.