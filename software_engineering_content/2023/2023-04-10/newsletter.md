## The Externality Trap
In life and careers, things happen that we don't control. Let's call these things externalities. When something happens, we naturally look for the root cause. This is alright and is the basis of one of humanity's most excellent tools, causal reasoning. With conclusions drawn from our root cause analysis, we can estimate how our choices affect future outcomes. Truly powerful, but when the root cause is something outside of our control, we will sometimes end our analysis before deciding how to act. This mental state is the externality trap. We can avoid the trap by choosing what to do and radically impact the possible shape of future outcomes. 

Let's pretend that you didn't get a promotion. Your analysis may have found that the root cause was a poor company policy or that your manager was unfair or uninclusive. If you stop here, you are trapped. Unable to act, you can't improve beyond this point. By finding your next course of action, you break free of the trap and affect the shape of future possibilities. 

## Story Time
Let me give an example from my career. I helped build a successful SaaS product during my stint as a DevOps engineer. We were an early adopter of the  AWS cloud and, as a result, ran operations out of the very first region US-EAST-1. This region is the oldest (meaning has the most experienced staff), has the most availability zones (isolated data centers), and boasts availability and durability numbers well within the SLA's. Everything should be hunkey dory, but even so, we routinely had outages at the AWS level that made management and our customer question if our public cloud strategy made sense.  

AWS going down was most definitely an externality. I couldn't do anything to change that. I would have been trapped if I had stopped thinking about the problem. Instead, I chose to focus on what I could affect. I took a deeper look and found a couple of things that would help. First, I decided to stop building SaaS applications in US-EAST-1. That decision alone has cut the number of AWS-caused outages for my applications in half. 

Second, I started having better conversations with management and customers about what uptime and durability numbers mean and how much it would cost to engineer solutions exceeding AWS SLAs. That choice often garners appreciation and understanding where none existed before. 

Avoiding the externality trap by focusing on things I can affect has yet to fail me. There have been times when externalities prevented me from reaching a goal, but by blocking my path, I could continue the journey in a direction I hadn't considered before.

I hope you take ownership of your career, and avoid the externality trap by choosing to act. 

It's your career. Own it!

---
Copy

Break free from the #ExternalityTrap! Learn how to take control of your career by focusing on what you can change. Don't let external factors hold you back. Read more here: [Article Link] 🚀

Stuck in your career? Discover how to escape the #ExternalityTrap and take charge of your future by embracing actionable steps! Check out our latest article for inspiration: [Article Link] 💼

Trapped by externalities? Transform your career by overcoming the #ExternalityTrap! Find out how to focus on the things you can control and unlock your potential. Read on: [Article Link] 🌟

Unleash the power of choice in your career! Avoid the #ExternalityTrap by focusing on what you can control and make an impact. Discover the secrets to career success here: [Article Link] 🔑

