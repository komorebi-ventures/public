The greatest risk we face in software development is that of overestimating our own knowledge.

--Jim Highsmith.

---
taken from: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66