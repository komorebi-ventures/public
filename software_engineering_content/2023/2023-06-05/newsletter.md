In the US corporate world, we use a lot of the learning from our military in the organization and operation of our companies.

That likely sounds strange to most folks even those who have worked in the corporate world for many years but its true. 

You see it was with the military that we learned how to organize huge numbers of people toward a common goal. It was also with the military that we learned to run a global supply chain. 

War is a horrible bussiness. I would to God that humanity would put down all their weapons of war and never take them up again. Regardless I am greatful for those who have taken up arms and given their all in the defense of my family. 