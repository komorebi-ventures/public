I am grateful for beautiful fall days. 
#givethanks 

---
What's the most important skill for a software engineer once you can actually, you know, engineer software? 

Communication!

If you're not sure where to start checkout this post by John Crickett (Note he also has a full on course on communication too). 

You’ll be glad you did. 

https://www.linkedin.com/posts/johncrickett_7-tips-on-how-to-communicate-effectively-activity-7115236650204418048-taEA?utm_source=share&utm_medium=member_desktop 

---

A computer lets you make more mistakes faster than any other invention with the possible exceptions of handguns and Tequila.

-- Mitch Ratcliffe

found on https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr 

---

After a grueling day at work, a programmer's wife called and asked him to swing by the store on his way home and pick up a loaf of bread. She added, "And if they have eggs, get a dozen."

When he got back home, he lugged in 12 loaves of bread and placed them on the kitchen counter. Baffled by the bewildered and slightly irritated expression on his wife's face, he exclaimed, "What? They had eggs!"

#fridayfunnies 

adapted from this page: https://www.galvanize.com/blog/the-best-software-engineering-jokes-on-the-web/

---


