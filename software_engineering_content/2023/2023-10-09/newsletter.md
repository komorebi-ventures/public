
# Rethinking Business: These Innovators Demonstrate The Plethora of Our Choices

Have you ever noticed how so many US businesses are structured nearly identical?

A small group of top level people make all the critical decisions, and the company grows a seemingly infinite number of management layers as it spirals into an incomprehensible beast that moves forward at the expense of its employees, its customers, and the world around it. 

In fairness it does provide a means of organizing huge groups of people and moving them in mostly the same direction, but is it the only way? Does it have to be this way? Do people have to be treated as disposable? Do companies need to focus on profit above all else? Are those with fiduciary duties to shareholders the only ones capable of making impactful decisions for a company? Do company life expectancies need to continue to decline?


NO!

When it comes to building a business there are as many organizational options to choose from as there are grains of sand upon the beach. Every one of which comes with trade offs.

Let me highlight three software companies that are courageously defying the norms, looking for a better way to build a business, and avoiding the worst parts about modern corporations.

## Valve

Valve is interesting because they have no formal hierarchy, job titles, or product/project management. They self-assemble into teams and projects based on the good ideas of individual contributors and those individuals ability to convince others the work is worth doing and is the right choice for Valves customers.
## Netflix

Netflix, has a massively hierarchical organization, except they have developed a culture that allows them to distribute decision-making authority throughout the company. That includes giving people at low levels the right to enter into contracts and spend tens of millions of dollars exercising their job function for what they believe is in Netflix's best interest.

## 37signals

37signals feels like the most contrarian to the group as their standard practices fly in the face of virtually every software engineering norm today. They only grow in employees when they need it for a new product. They stay just big enough to achieve the founder's objectives and no more. They do that while running a massively successful SaaS offering, built as a monolith, hosted in colos, with a project management framework of their own creation, without a sales team, and without forcing their existing customers to upgrade when they release a new version of their products. 

## tl;dr
A successful company doesn't have to follow the default patterns common throughout the corporate world. You are not guaranteed success because you copied the default pattern, but you are guaranteed to have to deal with the downsides of the choices you made. The next time you try to solve a problem for your business that seems to call for a commonly used practice, take the time to review the tradeoffs you are making and be ready to deal with consequences of your decisions.

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/more-than-one-way-skin-cat-robert-mccleary/). 

---
Whenever you're ready, there are three ways I can help you:

Let's Collaborate: I'm on the lookout for exceptional companies to partner with in various capacities, whether that's full-time employment, part-time advisory roles, or one-time consulting services. If you're interested, schedule a [brief call](https://topmate.io/robert_mccleary/450909) with me today.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Gain insight from the books that have shaped my thinking. Buy [The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi) today! 

Maximize your brand visibility by sponsoring this newsletter. Connect with an engaged audience in the tech industry. [Set up a time today](https://topmate.io/robert_mccleary/450948) to discuss the opportunity.

---

# Teasers

Teaser 1 - Intriguing Preview:
🚀 Uncover the Future of Business! 🚀
Ready for a mind-blowing revelation? Our upcoming article will challenge everything you thought you knew about business organization. Learn how groundbreaking companies like Valve and 37signals are reshaping the corporate landscape. Be the first to explore these innovative approaches by signing up for our newsletter today!

Teaser 2 - Empower Your Business Vision:
🌟 Ignite Your Business Transformation! 🌟
Are you tired of the same old corporate structures? Discover a new world of possibilities in our upcoming article. We're unveiling game-changing insights from industry leaders that can revolutionize your business. Don't miss out! Join our newsletter and be at the forefront of the business evolution.

Teaser 3 - Join the Business Revolution:
🌐 Redefine Success with Us! 🌐 Get ready to break free from the corporate mold! Our upcoming article explores how businesses are rewriting the rules for success. Stay ahead of the curve by subscribing to our newsletter. The business revolution starts here!

---

# Copy
Social Media Post 1 - Thought-Provoking Snippet:
🚀 Ready to Challenge the Status Quo? 🚀
Dive into our latest article and discover how innovative companies like Valve, Netflix, and 37signals are redefining success in business. Prepare to be inspired and rethink your approach to organizational structure. Read now! #BusinessRevolution #Innovation

Email Newsletter Announcement:
Subject: 🔥 Explore the Future of Business - Our New Article Is Live!

Dear [Subscriber's Name],

We're excited to share our latest article that's making waves in the business world! Discover the groundbreaking strategies and unconventional approaches used by companies like Valve, Netflix, and 37signals to achieve unparalleled success. Get ready to challenge the norms and revolutionize your business. Read now: [Link to the Article]

Stay ahead of the competition and subscribe to our newsletter for more exclusive insights. Together, let's shape the future of business!

Best Regards,
[Your Name]
[Your Company]

Website Banner Ad:
[Image of Engaging Visual]

🔥 Explore Business Beyond the Ordinary! 🔥

Discover the future of business with our latest article! Learn how industry pioneers like Valve, Netflix, and 37signals are reimagining success. Dive into groundbreaking insights that can transform your organization. Click here to read now and stay at the forefront of innovation.

[CTA Button: Read Now]

These pieces of copy are designed to pique curiosity and drive traffic to your article by highlighting the innovative content and its potential impact on readers' businesses.