I am greatful to have been born in the USA!

#givethanks

---
Ideas are worthless without great execution.

Likewise perfectly executing a terrible idea will equally get you nowhere.

For a long time, I thought that if my ideas were tremendous, everything would come up aces.

I know better now. If I want something to happen, I must find a way to make it so. The idea might be as little as 20% of what it takes to be successful. The rest is going to be the willingness I have to make the idea a reality and my ability to convince others I know what I am talking about.

---

Making the code you're working on easy to change reduces the toil for future engineers.

So you should always spend the time to make things easy to change, right?

WRONG!

There are lots of times when spending the time to make a system easy to change isn't worth it.

Like if the app you're working on has a well defined end of life, say like a one off conference app. Probably shouldn’t worry about making it easy to change.

Perhaps the app you’re working on doesn’t have any real customers yet. It's a bit soon to be optimizing for ease of future development.


tl;dr
Be pragmatic and make sure the awesome engineering best practices you implement actually make sense in your scenario before you do them.

---
UI is like a joke.


If you have to explain it, it's not very good.

#fridayfunnies 

adapted from:
https://www.galvanize.com/blog/the-best-software-engineering-jokes-on-the-web/

---
Excessive or irrational schedules are probably the single most destructive influence in all of software.
-- Capers Jones.

Taken from:
https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66