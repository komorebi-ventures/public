# The Power of Error Budgeting in Site Reliability Engineering (SRE): Balancing Change and Stability
A well implemented Site Reliability Engineering (SRE) strategy can help solve real world business problems. 

Your customers expect the software you provide them to be available. Most organizations keep a group of engineers whose sole purpose is to make sure that expectation is met. Unfortunately the number one cause of service failure is change. That change can be new features, new infrastructure, or even new users, but the result is almost always the same, customer impacting, newsworthy, business damaging failure.

The thing is,  developers in these organizations have had their livelihood tied to the speed of innovation they can sustain. They need to make continuous changes to the system to provide new and better products to the people paying the bills.

This paradox of development leading the charge to change and operations attempting to minimize system changes often leads to an internal civil war. If either side wins the business suffers. If you can't add new features or users to a product, you will eventually go out of business. Similarly, if no one can rely on your products, you will also go out of business. 

SRE resolves this conflict through error budgeting.

In Error budgeting the business comes together with feedback from their customers and agrees to a product's minimal acceptable availability. We call those service level agreements (SLA’s) . For example, if you know that clients feel they need to be able to access your software 99.9% of the time that becomes your SLA. As a business you then decide on a service level objective that is slightly higher than your SLA to prevent yourselves from failing to meet your commitment, let's say 99.95% uptime. If you are up 99.95% of the time, you can be down up to .05% of the time without consequence and that is your error budget. 

The business then agrees that as long as your uptime is within the .05% budget that development can move to change the system as fast as they like. Similarly if the system's availability falls below the budget the business agrees to stop normal development work and development partners with their SRE team to improve system availability and enable the development team to move at better speed in the future without breaking the budget.

It is a powerful concept, solving a substantial organizational dysfunction. Even if you are not planning on implementing an SRE strategy, you should seriously consider finding a way to balance the two extremes for long term business success.

---
This is V2 of this article.

You can find the previos version at https://www.linkedin.com/pulse/balancing-reliability-innovation-robert-mccleary/

---

"Are software changes causing more harm than good in your organization? Learn how a well-executed SRE strategy can bridge the gap between rapid innovation and system stability. 🌉🚀💻 Don't miss our latest newsletter! ➡️ [Link] #SiteReliabilityEngineering #BusinessSolution"

"Are your development and operations teams at loggerheads? Discover how #SRE's error budgeting can resolve the 'internal conflict.' Find out more in our latest newsletter. 🤝⚙️📈 ➡️ [Link] #DevOps #BusinessSuccess"

"Error budgeting: an innovative way to balance system changes and reliability. Want to find out how it works? Dive into our latest newsletter for all the insights! 🎯💡💼 ➡️ [Link] #SiteReliabilityEngineering #ErrorBudgeting"

Striking the perfect balance between innovation and reliability in your software can seem like an uphill battle. Explore the power of the Site Reliability Engineering principle of error budgeting in this week's LinkedIn Standup. Unlock your business success today! 🗝️🚀🔒 ➡️ [Link] #SRE #BusinessGrowth
