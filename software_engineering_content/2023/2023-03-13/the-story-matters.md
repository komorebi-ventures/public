The story you tell yourself matters. 

The difference between saying, "I have to go to work today" and "I get to go to work today" is palpable. It can mean the difference between finding solutions or spinning your wheels. The difference is between feeling like you just wasted the day or feeling like you accomplished something important. 

Ultimately, both perspectives are true, but the latter is more helpful. 

You get to decide how the story is told.
 
Make it a good one!

---
Previous versions of this one:
https://www.linkedin.com/posts/robertmccleary_leadershipskills-psychology-communicationskills-activity-6853756871392796672-g7mg?utm_source=share&utm_medium=member_desktop 


