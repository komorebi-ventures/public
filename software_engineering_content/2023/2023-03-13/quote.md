Computer science education cannot make anybody an expert programmer any more than studying brushes and pigment can make somebody an expert painter.

--by Eric S. Raymond.

---
From: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66 