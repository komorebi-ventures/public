#1 - Arg Parsing

You can parse arguments coming from the command line using the args package in Dart.

Example:
``` Dart
//Import the argument parser.
import 'package:args/args.dart';

void main(List<String> arguments) async {
  //Create a new ArgParser to parse user-provided command line arguments.
  var parser = ArgParser();
  
 //Define a command line option in the parser. abbr: allows me to give it an abbreviated name of n, and defaultsTo lets me set a default value if the user doesn't use the option when invoking the command.
  parser.addOption('numberOfCards', abbr: 'n', defaultsTo: '10');
  
  //Parse the arguments and store them in the results.
  var results = parser.parse(arguments);
  print(results);
}
```

---
#2 - Assertions
In Dart, you can use assertions at test time to validate that a variable stays inside the ranges you expect. 

NOTE: Dart does not currently support using assertions in production code. I honestly think that is an oversight.

Example:
``` Dart
void main(List<String> arguments) async {
  var results = '251';
  // This assert will pass because the value of result will be greater than one. 
  assert(int.parse(results['numberOfCards']) >= 1,
      'Please input a number greater than or equal to 1');
  
  // This assert will fail because the value of result is greater than 250.
  assert(int.parse(results['numberOfCards']) <= 250,
      'Please input a number less than or equal to 250');
}
```

---
#3 - Question on how to instrument command line utilities?

The other day I built a dinky command line app that grabs some data from an API.

I will never use it in a production-type capacity, but being the engineer I am, it got me thinking.

How would you build proper observability into a command line utility like the one I described? 
