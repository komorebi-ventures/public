When it comes to sick leave or PTO It shouldn't matter why you need to spend time away from work. 

Sickness, sadness, grabbing a few more hours of sleep because life happened in the middle of the night. The reasons are irrelevant. 

We need to trust people to be the arbiters of their own needs. 

At some point, someone's needs could cause problems for the rest of the team. Deal with that when it comes up, and don't worry about the rest.

---
Previous Posts:
https://www.linkedin.com/posts/robertmccleary_self-care-isnt-selfish-no-one-should-have-activity-6829430524277252096-55Um?utm_source=share&utm_medium=member_desktop