One of the downsides of the increase in remote work has been an increasing likelihood that you are working all the time. 

When everyone was in the office, many of us could easily separate ourselves from the frenetic pace of work when we stepped out of the office doors. 

Now there are no doors to step out of, our email and our chat are on our phones, and the space we usually use on our personal computers automatically signals that we "should be working," so we log on without thinking about it.

Unfortunately, too much of that can cause problems for people's mental health, so we have to engineer our environments to better care for ourselves and our teams.

The most helpful thing I have found to curb this trend is don't send messages after hours if it is not an emergency. We often find ourselves with a thought we need to get to our teams, so we head to chat or email, start writing it up, and inevitably hit send. This creates a chain reaction of replies that pulls many folks into work activities when it could have waited. 

I am not saying when it's in your head, you shouldn't write things up, but I am saying that you shouldn't send them right away, or if you do, you should schedule it to deliver when everyone would be working to avoid unintended consequences. 

---
Related posts
https://www.linkedin.com/posts/robertmccleary_amazon-people-mentalhealth-activity-6812733175845023746-8qfI?utm_source=share&utm_medium=member_desktop

