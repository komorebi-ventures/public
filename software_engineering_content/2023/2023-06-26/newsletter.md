# What is Site Reliability Engineering?

You know how every conversation with someone new eventually leads to them asking, "What do you do for a living?". It turns out that despite the sincerity of the question, day-to-day technical details are out of the scope of those kinds of conversations. It took me years to figure that out, so now I say I am a software engineering manager, and the conversation moves on magically.

If you are reading this, you likely want a little more detail than that. I am a software engineering manager leading a Site Reliability Engineering (SRE) team. 

Good enough? 

No? 

Ok then let's cover the high-level ideas around what SRE does .

Reliability is the most critical feature of a software system. The logic is simple if your customers can't rely on your software, it's the same as if you didn't write it. This idea is so basic that I have heard top engineering leadership dismiss the discipline entirely because “reliability is table stakes”, or “non-functional requirements [like reliability] are always assumed to exist in a system”. 

My experience differs significantly from either of those statements. System requirements like reliability don’t simply spring into existence without designing it that way. Additionally the longer a system is operated and updated the more and more and more unreliable it will become without consistent and thorough engineering put into ensuring its reliability.

Site Reliability Engineering combines architecture, system design, software engineering, and software operations. We view a software system from a holistic viewpoint so that we can engineer reliability in every facet.

SRE thinks of operations as a software problem. A healthy SRE organization ensures that operational tasks are executed and builds the system's ability to take care of these details independently.

How SRE accomplishes its work is the subject of another newsletter, but I hope this was enough detail that the next time someone tells you they are an SRE, you feel like you know what that means.

---
V2. Previous Version
https://www.linkedin.com/pulse/what-site-reliability-engineering-robert-mccleary


---

"Ever wondered what goes behind the scenes when a software never crashes? Unveiling the mysteries of a #SiteReliabilityEngineer's role in our latest newsletter. Don't just nod next time someone says they're an SRE, truly understand.👩‍💻🚀#TechTalks #SRE"

"Sure, you know what a software engineer does. But what about a Site Reliability Engineer? In this weeks LinkedIn Standup we're shedding light on the unsung heroes who ensure your apps don't let you down. 

Click below to read it now! 

💡🛠️ #SRE #SoftwareReliability"

"In our new #newsletter, we unpack the misunderstood realm of Site Reliability Engineering (SRE). They're the reason why your software remains reliable day in and day out. Get to know them better.🔧🌐 #SRE #TechInsights"
