I am grateful for healthy food that strengthens me. I especially love eating Lebanese hummus with broccoli. 

What healthy foods are you grateful for? I would love to hear about your favorites or any stories you would be willing to share about how good food makes your life better. 

Want to know “WHY!?” I share my gratitude on LinkedIn? Check out this week's article from the LinkedIn Standup to learn why I practice gratitude in public. 

#givethanks

https://www.linkedin.com/pulse/givethanks-robert-mccleary-ztggc

---

I am grateful for the people and companies that gave me a chance to learn with them and be a part of their success. 

What people, companies, or projects are you grateful to have been able to work on and with? I want to hear any stories you're willing to share about how they made an impact in your life. 

#givethanks

Curious about the 'why' behind my gratitude posts on LinkedIn? Dive into this week's article from the LinkedIn Standup for an insightful peek into why practicing gratitude in public can make a difference. Discover the power behind these posts and join the conversation!

---

🍂 For me Thanksgiving is a time to choose to be grateful for what you have, and connecting with people you love over the best foods. It's great because it's so simple.
 
I am grateful for Thanksgiving! 🙏

Which holidays bring you joy and gratitude? Share your favorite holidays and why you're grateful for them in the comments below!

🌟 hashtag#givethanks hashtag#ThanksgivingMemories


Ever wondered about the 'why' behind my gratitude posts on LinkedIn? Explore this week's article in the LinkedIn Standup for an enlightening glimpse into why sharing gratitude openly can create a meaningful impact. Uncover the profound influence behind these posts and become part of the conversation! Let's explore the transformative power of gratitude together. 🌟 hashtag#GratitudeInPublic hashtag#LinkedInInsights

---

The written word has been a true gift in my life. Through the shared knowledge of countless people I’ve been able to make impactful choices that otherwise would have been beyond me. 

What writings have influenced your life? Share your insights in the comments below. 

hashtag#givethanks 


Curious about the 'why' behind my gratitude posts on LinkedIn? Dive into this week's article in the LinkedIn Standup for an illuminating peek into why openly sharing gratitude can have a meaningful impact for people. Discover the deep influence fuelling these posts and join the discussion! Let's embark on a journey to explore the transformative force of gratitude together.

---

Knowing you read these posts means a lot to me. I love writing, and the idea that I am helpful is fulfilling. I still need the depth that only real-life connection provides but these interactions with you genuinely brighten my day.
Thank you!

Warm regards,
Robert


#givethanks

