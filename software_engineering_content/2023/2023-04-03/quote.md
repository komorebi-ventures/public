“Time pressure gradually corrupts an engineer’s standard of quality and perfection. It has a detrimental effect on people as well as products.” 
-- Niklaus Wirth.
---
From: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66