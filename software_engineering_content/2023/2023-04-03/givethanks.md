Do you ever jump on LinkedIn and immediately feel depressed by the stories on LinkedIn News and the nonsense in your feed?

I do.

There is pressure here (and in western society in general) to focus on and share all the terrible, no good, very bad things happening in the world around us.

To avoid this, I use the notifications system to alert me to things worth reading and leave the scroll to stronger mortals.

It doesn't always work, but it is a good start.

In defiance of that trend, I show gratitude at least weekly.

Why? When we take the time to consider things and find something to be grateful for, we improve our happiness, and the effect extends to those with whom we share our gratitude.

So what am I grateful for? I am thankful for the vast amount of human knowledge that people freely share over the internet. It has changed the trajectory of my life. 

Want to buck the status quo with me? Why don't you #givethanks in your next post? 

pervious version:
https://www.linkedin.com/posts/robertmccleary_givethanks-activity-6930553449835048960-_Rni?utm_source=share&utm_medium=member_desktop