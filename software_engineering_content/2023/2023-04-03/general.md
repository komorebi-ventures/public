Are the decisions you make based on emotion or rational thinking?

For me, emotion is the honest answer most of the time. It could be even 99% of the time.

Sometimes my rational evaluation changes how I feel about a situation. I usually decide based on how the matter feels and then rationalize why I chose a given behavior.

I can change this around. It's possible to listen to emotions and ensure they have a healthy outlet but not be ruled by them. They are just one part of our capacity to reason and keep us safe when there is no time for deliberation.

What about you? Do you think that your decisions are primally rational or emotional?

previous versions:
https://www.linkedin.com/posts/robertmccleary_what-if-every-decision-we-make-isnt-rational-activity-6922910874680664064-F8FJ?utm_source=share&utm_medium=member_desktop
---
The university path to starting a software career is diminishing.

It's becoming faster and cheaper every day to get started in other ways.

And I love it. Why?

I love that people can try a new career with less investment than ever before.

I love that, as an industry, we are willing to consider developers who came to the profession by various means.

My best recommendation for these industry newcomers? Learn everything you can from the people who have been in the field longer than you. They can help you avoid big mistakes and make your new career less stressful and more fun.

For those of you who have been in the field a while, do you have any advice for someone just starting a software career?

pervious versions:
https://www.linkedin.com/posts/robertmccleary_starting-a-software-career-via-university-activity-6925460082121945090-VuN7?utm_source=share&utm_medium=member_desktop
---

Do you suffer from the fear of taking time off (FOTTO)?

Sometimes significant changes happen during leave. While away, many things at the company could change, leaving people wondering if it's the right time to take time off.

In the current era of mass layoffs in tech companies choosing to dismiss team members on PTO, medical, and parental leave has only made FOTTO worse.

I recommend accepting that most of these changes would have happened even if you were there. Your presence wasn't required, and preventing yourself from taking needed breaks because of things you can't control is the perfect start to your next burnout cycle.

What do you think? Do you suffer from FOTTO?

pervious versions:
https://www.linkedin.com/posts/robertmccleary_do-you-suffer-from-fotto-a-colleague-of-activity-6926923692237074432-C-Rn?utm_source=share&utm_medium=member_desktop

---

I stumbled upon a concept in a book I read a while ago (maybe it was the 4-hour workweek). It described two types of stress in our lives. It called them distress and eustress. Distress is the kind of thing we usually associate with stress, and it means something that reduces your strength. On the other hand, eustress makes you stronger as you go through it.

For example:
A boss criticizes you == distress.
Exercising at the right frequency and intensity == eustress.

Most eustress can become distressed if we are not honest with ourselves and mindful of outcomes. That career-advancing opportunity stretching you to be better than ever is eustress until you stop taking care of your physical, mental, and social health to pursue it.

Similarly, depending on our attitude, some distressing events can even act like eustress. For example, if I had a particularly unflattering review at work, I could feel like my job is in danger and be diminished by it, or I can take the feedback and use it to become better than I have ever been. The only difference is the way I chose to perceive it.

I encourage you to find ways to limit the distress in your life and purposefully fill the space with eustress. Sometimes it is as simple as looking at a situation from another perspective.

What do you think? Is the adage "what doesn't kill you makes you stronger" true?

previous versions:
https://www.linkedin.com/posts/robertmccleary_i-stumbled-upon-a-concept-in-a-book-i-read-activity-6930274979338973184-DDH7?utm_source=share&utm_medium=member_desktop

---
I have learned a lot from great books in the last three and a half years.

The top three I would recommend to others are:
Software Engineering at Google
Never Split the Difference
The Obstacle is the Way

Do you have any I should put on my wishlist?

Previous versions:
https://www.linkedin.com/posts/robertmccleary_i-have-spent-a-good-deal-of-time-learning-activity-6931260255339249664-WHAV?utm_source=share&utm_medium=member_desktop

---
In software development, should you be a specialist or a generalist?

I have long been in the generalist camp. Why? One of the things I love about tech is its constant change and sheer size of possible things to learn. I get a kick out of learning.

A while ago I read an interesting take on the question have permanently changed my perspective.

In his book "The Complete Software Developers Career Guide," John Sonmez takes the stance of why not both?

Specifically, he says that you should come off as a specialist in the way you market and brand yourself. Not a .net dev but a backend .net dev with expertise in entity framework and Linux environments. Then keep your generalist capabilities by exposing yourself to various things across tech.

What do you think? Should you be a specialist, a generalist, or both?

previous versions:
https://www.linkedin.com/posts/robertmccleary_softwaredevelopment-softwareengineering-sitereliabilityengineering-activity-6931974129902374912-RxZP?utm_source=share&utm_medium=member_desktop

---


