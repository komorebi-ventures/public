# Software Career Options
In Software Careers, There Are a Lot of Options
When I first started looking for a job out of college, I assumed there was just one job in software, a developer. I was wrong. Software companies, like any other business type, require many different tasks for success. When a company is large, specialists undertake many of these tasks. 

NOTE: It is rare for all specialties to report through technical leadership. For example, sales and support often have a completely separate leadership hierarchy. 

The goal here is to open your eyes to the possibilities available as you think about a career in software.

## There are Levels to Most Technical Specialties
Most companies split up technical specialties into the corporate equivalent of a rank. Below are common ones for technical team members in order of ascendancy. Some people and organizations have tried to give these levels a standard definition, but I have never seen two companies define them precisely the same way.
Specialty Levels:
- Junior
- I
- II
- III
- Senior I
- Senior II
- Staff I
- Staff II
- Principle I
- Principle II
- Fellow

## Job Descriptions
Often each level will be defined by a set of skills or attributes. An example job description for a junior web developer might look like this.

A successful candidate will have the following:
A bachelor's degree in Computer Science, Electrical Engineering, a related field, or commensurate professional experience 
Proficiency in HTML, JavaScript, and CSS
A basic understanding of a three-tiered web architecture
Familiarity with *nix-based operating systems

When the description is for a higher-ranking job, you will often see a time-in position or total time-in career requirement. 

A web developer I might look something like this:

A successful candidate will have the following:
A bachelor's degree in Computer Science, Electrical Engineering, a related field, or commensurate professional experience 
Two to four years of hands-on web development experience.
Proficiency in HTML, JavaScript, and CSS
A basic understanding of a three-tiered web architecture
Familiarity with *nix-based operating systems


The assumption is that one obtains certain ineffable but desirable qualities with years of experience in a specialty. 

The time-in position model does improve the chances of success for individuals taking on a new level for their specialty. Unfortunately, it also has its downsides. For instance, only a few companies hire at the entry-level (aka junior). Junior-level candidates take significant amounts of training before operating effectively in an organization. To support that kind of training, organizations need to gain the required experience to provide that training. This lack of training infrastructure leads to a common complaint from new university graduates that every "entry-level" position requires two to four years of experience. The truth is that these graduates are rarely looking at actual entry-level positions. This model also prevents capable people from moving forward when they don't have the correct years of experience.

## Software Career Specialties
This section describes broad work types and their specialties.

### Writing Software
*Software Developer* 

Writes code with a short expected life span or code unlikely to change after delivery. These tend to be heavy on the research side of R&D and often are the beginning of the code writing career arc. All software projects start as software development projects.

*Software Engineer*

 Writes code focusing on making systems that will live for years. Software Engineers build the system so that changing them is sustainable throughout its lifetime. Most SaaS companies hire for these. Accounting for continually altering an operational system is complex. Often Software engineers are hired from a pool of experienced software developers. Many successful software development projects become software engineering projects that continue to be improved and maintained to maximize their value. Software engineering is far more expensive than software development, so only some projects should become software engineering projects.

*Software Architect* 

Architects are specialist software engineers that have become adept at understanding ways to optimize code for performance and scalability. Often They are used as consultants at every phase of a software engineering project. They consider all the necessary details of building and maintaining the project, focusing on patterns.

### Testing Software
*Quality Assurance (QA) Analyst*

Focus on building test cases for the various software features. They also manually run through the test cases to verify everything works as expected. 

*Software Engineer in Test (SET)* 

Writes code focused on automatically testing other code. SETs may also build platforms that make it easy for software engineers to test their code. 

### Operating Software 

*Systems Administrator*

Traditional non-software development roles. They do everything required to make a piece of software available to end-users, including building and maintaining servers, networks, storage devices, etc. In the past, they did this manually, but today they often use automation as a force multiplier. 

*DevOps Engineer*

DevOps Engineers are Systems administrators who are included directly in the engineering process. Responsibilities range from running the servers needed for a software product to managing the build process and engineering platforms that make everyday software engineering tasks faster and easier.

*Site Reliability Engineer (SRE)*

Writes code that manages the operational details of a software system. Their work tends to be heavy on the automation of manual tasks. Still, they often improve the software to resist failure, scale, and perform better. These are similar to software architects but focus on operational features. SREs tend only to exist in high-scale engineering projects.

*Build Master*

Build masters are experts in builds, pipelines, compilers, etc. They ensure the software build process is always in working order while optimizing speed and reproducibility. 

*Cloud Architect*

Cloud Architects have a deep understanding of the various offerings of the cloud. Cloud Architects help plan software engineering projects that intend to use the cloud to deliver their services. 

*Supporting Software*

Customer Support Representative
CSRs are frontline workers that support a software product. They will usually interface directly with customers who have questions or have found bugs in a software product. They attempt to help the customer resolve the issue by training or engaging the engineering team to fix a bug.

### Delivering Software
*Customer Success Manager*

These tend to be experts in using a given software product and are assigned to customers to help them learn and optimize their software usage. 

*Implementation Specialist*

Tend to be experts in installing and configuring a specific software stack. They work with clients to customize the software to fit their needs. They sometimes work as software developers who focus on building one-off software features for a given customer.

### Selling Software
*Account Manager*

Account managers establish relationships with potential clients. They learn the client's needs and try to show a prospect how their software will meet those needs.

*Sales Engineer*

Sales Engineers are experts in using a given software offering. They provide demonstrations to a potential client. They often work closely with the account manager to customize a demo to the client's interests. 

*Sales Software Engineer*

Sales software engineers work closely with sales engineers. They develop missing features required during a demo for a prospect's use case. In other words, they produce vaporware in the hopes of increasing sales.

### Rare or Impossible Specialties
*Full Stack Engineer*

The full-stack engineer is the unicorn of the software career specialties. The title exists, and people have it, but it implies a one-person engineering team. Although there may be people capable of doing everything mentioned above, I have yet to meet someone good at every specialty. Early-stage startups want this type of engineer because they can't afford to hire a fully formed cross-functional team. 


### Leading Software Teams
*Team Lead*

Are usually the boundary between individual contributors and managers. They are typically senior and have a good deal of experience with the technical aspects of a team's responsibility. They can still contribute as a technical resource but may also be focused entirely on helping team members excel. 

*Manager*

These usually have multiple leads reporting to them. They serve as the first level of middle management between the lead and director levels of the company. They are often involved in tactical planning to help their teams understand and meet business objectives.

*Director*

Usually have multiple managers reporting to them. They serve as a middle manager between them and the vice president level. They often will be involved in some of the tactical planning and some of the strategic planning. Depending on the company, this can be the first executive management level. Directors and the organizations they lead tend to be the workhorses of a company.

*Vice President*

Often have multiple directors reporting to them. They will generally not be involved in the tactical planning for their reports but instead focus on the mid to long-term strategic planning for the organizations below them. 

*Chief Technical Officer (CTO)*

These are the highest authority in a technical organization. They generally are focused on long-term strategic planning for everyone below them. They are usually adept at communicating with the non-technical C-level executives and the board of directors and translating directives from these groups into something intelligible for the teams below them.

*Chief Architect*

These usually head the other company architects. They focus on building a company's technical strategy and usually complement a CTO who has been out of the trenches too long to know the right call for technical choices. Chief Architects will often spend a reasonable amount of time with the CTO, other executives, and the board of directors to understand the big picture of business needs and use those relationships to develop the technical strategy that will allow the R&D organization to make sure what they are doing and the building is helping the business move in the direction the company wants to go.

## Conclusion

As our exploration of software career options ends, remember that each individual's journey will be unique. I have provided a general overview of the different roles and specialties within the software industry. When determining your path, it is essential to reflect on your strengths, passions, and goals. The software landscape is vast and ever-evolving, providing endless opportunities for growth and exploration. As you step into this exciting realm, embrace the challenges and adventures that await you, and discover the fulfilling and rewarding career ahead.

---
🚀 Discover the vast world of #SoftwareCareers! Our comprehensive guide explores various roles, specialties, and levels within the industry. Find your perfect fit and kickstart your tech journey today! 💻🌟 [Article Link] #TechJobs #CareerGoals

🤔 Confused about software career options? In this week's LinkedIn Standup I break down the specialties, roles, and levels to help you navigate the exciting world of software careers. Unleash your potential now! 🔥 #SoftwareJobs #CareerAdvice

💼 From Software Developer to CTO, the software industry offers countless opportunities! Unlock your future in tech with our insightful guide to software career options. Begin your journey today! 🌐 [Article Link] #TechCareers #JobSearch

🌟 Ready to level up your software career? Learn about the wide range of roles, specialties, and levels in the software industry with our ultimate guide. Your dream tech job awaits! 🎯 [Article Link] #SoftwareIndustry #CareerPath