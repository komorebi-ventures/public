# Churn or Consistency: The UI Dilemma in Software Development

In software engineering, we generally treat our API layer as a contract with our downstream consumers. It's easy to see why, we have all been bit when an API we rely on unexpectedly changed and we were left alone to pick up the pieces of our broken dreams. 

In a real way UI is to our end users what the API is to us. At best every change forces the user to relearn things they already knew how to do. At worst our changes end up breaking valuable workflows our users relied on.

The average software engineer is not the average end user. We tend to love living on the edge of what is possible, trying new and innovative things, and squeezing out the next increment of improvement to our workflows. In other words we are far more risk-tolerant than our users tend to be. Our users want to make a phone call, order a pizza, or track their weight. They don’t care about marginal improvements to the workflow; they want predictability.  If the software they use changes it's going to need to provide exponentially more value to justify relearning something. 

A case in point:  a surprising number of people are still using the last perpetual licensed version of the Adobe Creative Suite. Yeah, the subscription version has massive amounts of added functionality, and you can pay for it only when you need it, but it changes so fast that it causes a lot of rework for those who have adopted it. Rework the old-school users can skip, and actually accomplish what they are trying to do.

A good example of people who take UI changes seriously is 37Signals. When they make massive changes to Basecamp they don’t just create a new version and migrate all the users, they create an entirely new system. They guarantee their users can use the old version of their products as long as they want to and provide an easy upgrade path for those who are willing to get the new hotness. 

## tl;dr
If we make UI changes fast enough with little enough value delivered, we create churn, so make sure you're evaluating the tradeoffs before your next big UI refresh. 

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/dont-forget-contract-we-create-via-ui-robert-mccleary). 

---
Whenever you're ready, there are three ways I can help you:

Let's Collaborate: I'm on the lookout for exceptional companies to partner with in various capacities, whether that's full-time employment, part-time advisory roles, or one-time consulting services. If you're interested, schedule a [brief call](https://topmate.io/robert_mccleary/450909) with me today.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Gain insight from the books that have shaped my thinking. Buy [The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi) today! 

Maximize your brand visibility by sponsoring this newsletter. Connect with an engaged audience in the tech industry. [Set up a time today](https://topmate.io/robert_mccleary/450948) to discuss the opportunity.

---

# Teasers


---

# Copy
### Engage Your Users, Not Frustrate Them! Discover the Impact of UI Changes in Software Development.

"Are your software UI changes leaving users bewildered and frustrated? Learn how to strike the perfect balance between innovation and user satisfaction in our latest article. Explore real-world examples and expert insights."

### Secrets from Adobe to 37Signals: Master the Art of UI Changes.

"Unlock the secrets behind successful UI changes! From Adobe's perpetual license users to 37Signals' revolutionary approach, our article reveals how to keep users delighted during transformations."

### User Retention Matters: Why UI Refreshes Require Careful Evaluation.

"Are UI changes causing user churn in your software? Dive into our latest article to understand why thoughtful evaluation is crucial. Learn from industry examples and make your next UI refresh a hit!"








