I am grateful that I have hope for a better tomorrow.

#givethanks

---

It’s not a bug; it’s an undocumented feature. 

-- Anonymous

This one came from
https://techvify-software.com/35-best-coding-programming-quotes/ 

---

Knock knock.

Race condition.

Who’s there?

#fridayfunnies

This one came from

https://www.altcademy.com/blog/the-best-jokes-about-programming-and-software-engineering/

---

Anyone ever told you “Good code is self documenting” so you don’t need to write comments?


I have. A LOT!


So if your code needs comments you just aren’t very good at your job right?


WRONG!


Code comments provide insane value when used to describe why your code does what it does not what your code is doing. It's the context that matters here.


tl;dr
Comments have not been added to every modern programming language because their creators are dumb copy cats, they exist to help capture context that the code itself can’t. 

---


