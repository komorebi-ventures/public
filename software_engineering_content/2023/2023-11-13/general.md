While at the bar a SQL query approached a pair of tables and asked, “May I join you?”

#fridayfunnies

Adapted from: https://www.databasestar.com/programmer-jokes/

---

There's this saying that I like to remember: “There are no million dollar ideas, only million dollar executions (and sometimes million dollar implementations, lol) 



Jokes aside, this truth really sticks for me. We often catch ourselves looking at super successful folks and thinking, 'If only I had come up with that first' or 'I had that idea ages ago.' But those thoughts are a bit misleading, aren't they? 

When we witness someone else's triumph, we lack the full picture of what it took to reach that point. Even if they spill the whole story, they might forget the bits and pieces that were crucial—things they didn't even realize were important. It wasn't just about a fantastic idea at the perfect time; it involved endless hours, days, weeks, months, and years of brainstorming, experimenting, failing, trying again, and adjusting the idea and their own efforts to make it a reality. There wasn't a crystal ball predicting their success, nor was there any guarantee when they started or during their journey. It was a relentless grind until, out of the blue, their idea became a part of the real world, and they reaped the benefits from it.



tl;dr

If you want to make something real, the idea is just the first step of a very long journey.
---

