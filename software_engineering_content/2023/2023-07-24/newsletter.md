# Is DevOps Dead or Will it be Reinvented?

Traditionally IT organizations had two main teams. Development and Operations. 

Development was tasked with designing and delivering software with new capabilities as quickly as possible. 

Operations was tasked with keeping the software that Development was building running so clients could use them.

From a command and control perspective, this split of responsibilities makes sense in a ton of ways. By specializing, the two teams should be able to improve their respective skill sets. Specializing also means that if there is a problem in one area, work can still progresses in the other.

It all sounds perfect. Yet another triumph of hyper-specialization in the information age, except when left in this state, a schism forms. It turns out that change is the number one cause of system failures. In this model, you have incentivized one team to change as much and as fast as possible, but another team to keep the system operational. The more successful the development team, the less successful the operations team, and vice versa. In the best cases, this leads to an arms race of sorts. Operations will implement "checks and balances" to prevent change from making the system unstable, and development, because they are being slowed down, will find ways around what was implemented. In the worst cases, this misalignment of goals starts an all-out political war inside the company, with each side blatantly harming the other and the business. Regardless of how bad it gets, the net result is too much time spent fighting and too little time accomplishing the company's actual goals.

DevOps came out of the Agile mantra of continuous improvement. Its goal was to unify the two teams' distinct concerns and abilities and prevent conflict. Both sides were needed to produce new software and keep that software stable. We could achieve both goals if we worked together to find ways to improve things a little each day.

Continuous improvement is a powerful concept, but it is not a concrete one. As a result, DevOps takes on a different meaning at every company because no two environments are exactly alike.

Today you have a seemingly endless number of voices, all describing DevOps in different ways. The vendors take advantage of that, each saying that you can get the benefits of DevOps in your environment by buying their tools. Similarly, management fast tracts the adoption of patterns and practices that worked for other companies but without evaluating if the problems those patterns solved even exist in their company and with no way of knowing if the new ways of doing things help. 

One pattern that has spread in popularity is hybridizing the two roles into full-stack engineers. The idea is that hyper-intelligent people can learn everything needed to build a software system and keep it running. The tradeoff for this choice is that they can do everything but never gain the speed and competence specialization provides. One downside of this is a larger and larger group of developers are vocal that they don't want to do operations work.

Another pattern that has emerged is the concept of Platform teams. The idea of the platform team is to take the operation specialization and allow them to create a product they provide to the company's developers that has all the reliability qualities the company's customers need without the developers needing to learn or do much to take advantage of all that built-in goodness for their software. The rise of platform engineering has led many in the industry to declare "DevOps is Dead!" and truly mean it. Unfortunately, the building of a platform without understanding what the development teams are trying to accomplish leads to its own set of inefficiencies. Very few companies come away winners in this pattern without the same kind of deep inter-team connection and continuous improvement that is the hallmark of the DevOps movement.

What does all that mean? It means even with all the hype of DevOps being dead and platforms ruling the world, many companies will choose to keep the DevOps patterns in place because it is still much better than returning to the infighting of the old world, or the inefficiencies of the new. That is okay. Similarly, other organizations will split things out again using the platform model alone. That is okay too.

The correct answer for your engineering problems is always context-dependent. Learn what you can from where you can but let the context of your environment temper your solutions.

tl;dr
## DevOps is Dead. Long Live DevOps!

---

This is v2 of this article. The previous version can be found [here](https://www.linkedin.com/pulse/devops-dead-long-live-robert-mccleary). 

---
Whenever you're ready, there are three ways I can help you: 

Consulting Services: Ready to transform your engineering organization?  Book a [quick call](topmate.io/robert_mccleary?utm_source=topmate&utm_medium=popup&utm_campaign=Page_Ready) with me today.

[The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi): Gain insight from the books that have shaped my thinking. Buy [The Good, The Meh, and the Ugly - A Book List](https://rjmccii.gumroad.com/l/fxxtxi) today! 

Maximize your brand visibility by sponsoring this newsletter. Connect with an engaged audience in the tech industry. [Set up a time today](https://topmate.io/robert_mccleary/450948) to discuss the opportunity.

---
# Teasers

**1**

Is DevOps Still relevant in the era of Platform Engineering? Join the LinkedIn Standup and be the first to read our latest deep dive 'What's Happening to DevOps? Will it Live, Die, or be Reinvented?' when its released. 

Sign up now!

2 

Ready to join the debate on the future of DevOps? Don't miss our next LinkedIn Standup! We'll be discussing our upcoming article, 'What's Happening to DevOps? Will it Live, Die, or be Reinvented?' – a fascinating exploration of DevOps, its alternatives, and their implications. Sign up today for a thought-provoking conversation!

3 

Want a sneak peek into our newest deep-dive: 'What's Happening to DevOps? Will it Live, Die, or be Reinvented?'? Join our LinkedIn Standup to explore the evolution, challenges, and future of DevOps in the tech world. Be a part of the conversation that's shaping the industry. Sign up now and stay ahead of the curve!

---
**1** 


🔥 Hot off the press! 'What's Happening to DevOps? Will it Live, Die, or be Reinvented?' is now live. Explore the evolution, controversies, and future of DevOps in our latest deep dive. Don't miss out on this chance to stay ahead in the tech game. Read it now!

2 

Our new article 'What's Happening to DevOps? Will it Live, Die, or be Reinvented?' has dropped! Find out why companies are choosing to keep the DevOps patterns in place, despite all the talk of its death. Get ready for some thought-provoking insights. Check it out today!

3 

Dive into the life, death, and rebirth of DevOps with our latest article, 'What's Happening to DevOps? Will it Live, Die, or be Reinvented?' Learn why the future of tech might not be as straightforward as it seems. Intrigued? Click to read now and join the conversation!