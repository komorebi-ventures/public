I am grateful for technology that makes it easier to do everything I need to get done. 

#givethanks

--- 
Create a post around this video by Eric Andersen.
https://www.linkedin.com/in/ebandersen/overlay/1635529369115/single-media-viewer?type=LINK&profileId=ACoAAAb0BmUBtUxtGTevtpCZmVHpGuW0lD1GFtE&lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3B2IwfE5d4R1%2Br7zCdeuW4Nw%3D%3D

This week I got to watch an awesome video made by Eric Andersen. Eric is Senior Software Engineer with more than 13 years of industry experience.

Have you ever noticed how every entry level position for software requires 2-4 years of experience? The question is how do you get 2-4 years of experience if all the lowest level jobs require it to start?

That’s where Eric’s video comes in.

In it he talks about three ways to get experience in a world where no one is willing to train you. 

At a high level they are:
- Partner with an Entrepreneur
- Volunteer to help a small business or nonprofit
- Build your own business
He also covers the pros and cons of each and some of the lower levels of details needed to pull each off.

If you or a friend are struggling with that conundrum take a look at Eric's Video below. 

https://youtu.be/t6F9418wuwE 

--- 
1 

As you build out your startup you will be tempted to work nights and weekends on top of a full work week to make the magic happen. 

You might even believe that this is the right choice because  in the startup phase there is simply more work to do than someday when your company is more established. 

In reality there will always be more work to do than can be done, no matter where you are in the journey of your company. 

Investing all of your time may help you mask your anxiety for the company's success but it's not going to improve your chances and it will negatively affect your health, your relationships, and your sanity. It will also set the example for everyone else you bring in to help you and they too will end up on a one way track to burnout.

Don’t get me wrong, there are times when night or weekend work will be necessary because of real circumstances but those are the exceptions not the rule, and many times can be engineered around anyways.  

If this sounds like you, clear your schedule tomorrow, and take a long hard look at your choices. If you're like many of the founders I know there is a good chance you can be more successful, and live a healthier and happier life by working on the most important thing in the limited scope of your chosen schedule.

You can engineer a sane work life for yourself right now, no need to wait for someday. 

#startupengineering #WorkLifeBalance #SustainableSuccess

2 

You may have found yourself burning the midnight oil and giving up weekends, hoping that this is the secret sauce to your startup's success.

Here's the truth – in any phase of a company's life, be it a fledgling startup or an established corporation, there will always be a surplus of work.

While devoting every waking hour may temporarily hush your fears about the success of your startup, it won't necessarily enhance your chances. Instead, it could harm your health, strain your relationships, and lead you towards burnout. This model also becomes the template for your team, leading them down the same destructive path.

While there are occasions that might demand extra hours, these should be the exceptions, not the norm.

If this is you, take a day off to reflect. I've seen many founders who've found greater success and happiness by focusing on the key tasks within a fixed schedule.

Begin crafting a balanced work-life for yourself today.

#startupengineering #WorkLifeBalance #SustainableSuccess

3

As an entrepreneur, the temptation to labor into the night and all weekend is strong.

It may feel like for a startup to succeed it requires non-stop work, however that overlooks a vital truth – regardless of your company's stage of growth, there will always be more tasks than hours in the day.

Investing every second of your time offers a false sense of security about your startup's success. Yet, it's unlikely to increase your chances of success, and is more likely to jeopardize your health, personal life, and mental stability. It also sets a precedent for your future team, steering them towards potential burnout too.

Though there will be instances when working overtime is inevitable, these should be rare and manageable.

If you're reading this and nodding, clear your calendar for a day. Reflect on your priorities. Many founders I've worked with have found more success, health, and happiness by working smarter, not longer.

Remember, the ideal work-life balance is not a distant dream, but an immediate possibility.

#startupengineering #WorkLifeBalance #SustainableSuccess

4 

When charting the course for your startup, you might think working long nights and weekends on top of your regular hours will bring you to your destination faster.

The truth, however, is that whether you're in the startup phase or leading an established company, there will always be a mountain of work.

Committing every minute of your day to your startup might momentarily ease your anxiety, but it won't necessarily boost your chances of success. Instead, it can be detrimental to your health, personal relationships, and overall well being, and also create a culture of burnout for those joining your journey.

While sometimes burning the midnight oil is unavoidable due to unforeseen circumstances, this should be an exception, not the standard.

If this resonates with you, take a step back tomorrow. Evaluate your decisions. Many successful founders I know have achieved more by focusing on priority tasks within a reasonable schedule.

Don't wait to create a balanced work-life. The time is now.

#startupengineering #WorkLifeBalance #SustainableSuccess



--- 

One of my most productive days was throwing away 1,000 lines of code.

-- Ken Thompson

--- 

There are only 10 kinds of people in this world: those who know binary and those who don't.

#fridayfunnies