Software and cathedrals are much the same; first we build them, then we pray.

-- Samuel T. Redwine Jr.

From: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66

To be a programmer is to develop a carefully managed relationship with error. There’s no getting around it. You either make your accommodations with failure, or the work will become intolerable. 

-- Ellen Ullman

From: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66