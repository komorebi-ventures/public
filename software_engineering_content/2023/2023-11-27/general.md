I am grateful for good books.

#givethanks

---

Plug this post: https://www.linkedin.com/posts/andrewfong_platformengineering-cicd-ai-activity-7122957306832572416-5EgF?utm_source=share&utm_medium=member_desktop 

Since the release of GPT-4 I have been trying to formulate a good way to express my concern about what we are facing.

Let me give a shout out to @Andrew Fong for finding a way to express it so succinctly. 

He is absolutely right. The speed of change the teams we support are now capable of with generative AI is exponentially greater than it was 12 months ago and it will only increase as more tools are introduced and adopted. 

We really need to rethink our assumptions and start preparing for a level of change we wouldn’t have needed to consider in our wildest fever dreams.

#devops #sre #platformengineering

---

Most software today is very much like an Egyptian pyramid with millions of bricks piled on top of each other, with no structural integrity, but just done by brute force and thousands of slaves.

-- Alan Kay

from: https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr 

---

I decided to solve a problem with Java. 

Now I have a ProblemFactory....


#fridayfunnies

adapted from: https://www.databasestar.com/programmer-jokes/

---

