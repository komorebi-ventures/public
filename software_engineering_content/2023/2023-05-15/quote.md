Program testing can be used to show the presence of bugs, but never to show their absence!
--Edsger Dijkstra

---
Taken from: https://www.comp.nus.edu.sg/~damithch/pages/SE-quotes.htm