A QA engineer walks into a bar and orders a beer, -1 beer, 50 beers, foobar beers, and NULL beers. The barman happily fulfills the orders he can and declines the rest.

A user walks into the same bar and asks the barman where the toilets are. 

The barman explodes, the bar burns to the ground, and the building collapses.

---
adapted from
https://upjoke.com/software-jokes