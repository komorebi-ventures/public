Good programmers use their brains, but good guidelines save us having to think out every case. 

-- Francis Glassborow.

---
From: https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66