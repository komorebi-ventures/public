# Generalist vs. Specialist

Should I be a generalist or a specialist? I have heard (and asked) that question repeatedly throughout my career. 
My answer is a resounding "Why not both?".

How can you have both? I do it like this.

My specialty is software engineering managment for site reliability engineering. I have deep experience in software operations and more than enough development experience to know that I want the machines to do all the chump work for me. I continually hone my skills, now from the perspective of helping other SREs do their best, and I am on the lookout for patterns and concepts that will improve my craft and benefit my company. Recently I have read two books, read dozens of blog posts, listened to a handful of podcasts, watched a few Youtube videos, and worked with my team almost daily to improve my specialist capabilities. My specialty is what I am known for and how I market myself professionally. It's how I provide demonstrable value.

At the same time, the sheer breadth of what is possible to learn enthralls me. In a similar time frame, I have studied python, typescript, svelt, google cloud platform, sales, marketing, business operations, human psychology, distributed systems, techno-social systems, AI etc. Most of what I am learning won't help me directly with my specialty. Still, they all give me a more extensive well of knowledge to draw from when interacting with other actual human people. 

In other words, learning things outside my expertise helps me relate to people outside of my specialty, and I find that extraordinarily valuable. 

Every once in a while, I connect two ideas that help me solve a problem in site reliability engineering that I had not been able to imagine before.

The tl;dr?
Being capable of providing above-average value by specializing is a killer way to progress your career. You can significantly augment that value by allowing your generalist curiosity some room to express itself. Diversity of perspective can be a source of value all by itself.

---

"Curious about blending generalist and specialist skills for a soaring career? Dive into this insightful read about being a 'T-shaped' professional. #CareerAdvice #Tshaped 🚀 👉 [Link to article]"

"Does specialization limit your professional growth or catapult it? What if you could have the best of both worlds? Unlock the potential of being a T-shaped professional in our latest article! #CareerGrowth #SpecialistVsGeneralist 👉 [Link to article]"

"Broaden your horizons without losing your expert edge! Learn the secrets of successful T-shaped professionals in our new piece. Time to evolve your career! #CareerEvolution #TshapedSkills 👉 [Link to article]"

"Struggling to choose between being a generalist or a specialist? Why not both? Uncover the power of being a T-shaped professional in our latest read. Let's shape the future of your career! #FutureOfWork #Tshaped 👉 [Link to article]"