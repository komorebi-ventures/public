Why does work feel crazy at so many companies? 

I don't know for sure, but my best guess is it feels crazy because we are choosing to make it crazy. 

How do you choose to make work crazy? Do you think solving this problem is as easy as making a different choice?

---

