My team is responsible for maintaining the 24x7x365 on-call rotation for multiple product lines at Filevine. 

It is challenging work and can take a severe psychological and physical toll on them. 

Some of the ways we try to reduce the effects of this part of our jobs are:
- Split the weekly rotation into two daily parts so no one will work 24 hours straight. 
- Adding mandatory comp time to either end of the rotation, so there is time to prepare for the rotation and time to decompress with the rotation ends. 
- We put regular effort into continually improving things, so the next rotation is less stressful than the last.
- We carefully choose what alerts are worth waking someone up to respond to.
- We separate our everyday and emergency communications so people can mute routine communications during off hours.

Have you seen any other effective ways to limit on-calls impact on engineers?

---
Previous version of this post:
https://www.linkedin.com/posts/robertmccleary_worklifebalance-activity-6902978299622952960-3l0e?utm_source=share&utm_medium=member_desktop 
