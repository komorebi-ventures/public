There is a genuine aspect in human phycology that once we learn the first bit of something, we feel like we know it all. 

When we continue to study the subject or run up against the limits of our knowledge when applying what we learned, we understand we were wrong.

We see this in our teenagers, we see it in our politicians, and online gurus (you know, like me...). The only way to avoid falling into its trap is to be humble enough to keep learning, admit when you don't know something, and remember the number of things you don't know will always exceed the number of things you do. 

---
Other Versions of this Post:
https://www.linkedin.com/posts/robertmccleary_devops-softwareengineering-culture-activity-6868978716496814080-GXpd?utm_source=share&utm_medium=member_desktop 

