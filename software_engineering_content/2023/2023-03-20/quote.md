Poor management can increase software costs more rapidly than any other factor.
-- Barry Boehm.

Taken from:
https://medium.com/geekculture/famous-quotes-about-software-engineer-everyone-should-know-9b946cd6ec66