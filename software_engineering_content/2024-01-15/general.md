I am grateful for being able to take life one day at a time.

#givethanks

Posted 1/16/24 : https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7153175793953574912-uRkr?utm_source=share&utm_medium=member_desktop 

---

Consider puligging this from Hussain https://www.linkedin.com/posts/hnaser_if-your-system-is-complex-dont-make-it-simple-activity-7149848298806231041-XTG0?utm_source=share&utm_medium=member_desktop

Another great post by Hussein Nasser. 

During one of the first DevOps conferences I attended I heard a quote that stuck with me. 
Complexity x Scale > Our ability to manage it. 
Basically the idea is that the more complex a system is the harder it is to manage at scale. I have yet to encounter a system where this isn’t true. 
We want to be honest about complexity and ensure our systems are as simple as they can be but no simpler. 

Hussein’s point about trying to make a system look simple not providing any real value is right on the nose. You should take a look at the rest of the post too.

posted 1/17/24: https://www.linkedin.com/posts/robertmccleary_if-your-system-is-complex-dont-make-it-simple-activity-7153525182484103171-VrEJ?utm_source=share&utm_medium=member_desktop
---

There are only two hard things in Computer Science: cache invalidation and naming things.
-- Phil Karlton

Taken from https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr 

posted on 1/18/24: https://www.linkedin.com/posts/robertmccleary_there-are-only-two-hard-things-in-computer-activity-7153915707947151360-M9ex?utm_source=share&utm_medium=member_desktop

---

Software Engineers love to solve problems. Don’t worry, if there are no problems available they’ll just create their own. 

based on a joke originally found here: https://www.galvanize.com/blog/the-best-software-engineering-jokes-on-the-web/

posted on 1/19/23: https://www.linkedin.com/posts/robertmccleary_software-engineers-love-to-solve-problems-activity-7154302361975279616-p1bP?utm_source=share&utm_medium=member_desktop



