I am grateful for good food that is good for me.

Tonight that includes oatmeal, with raisins, olive oil, butter, cinnamon, salt, and a little bit of vanilla. 

#givethanks

posted on 1/23/2024: https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7155707786180317184-W1eQ?utm_source=share&utm_medium=member_desktop 

---

Plug this post by Erik Andersen: https://www.linkedin.com/posts/ebandersen_programming-jobsearch-softwareengineering-activity-7154140475329789953-TqU9?utm_source=share&utm_medium=member_desktop 


🚀 Landing your first job in software development is no small feat—I've been there. It took me several hundred applications and months of perseverance to secure my first position. The most crucial lesson: don't let rejection convince you that you can't provide value. It's easy to get discouraged, but remember, persistence pays off!

If your a junior in the market for a new job 🎤 Erik Andersen consistently shares top-notch advice for juniors. His interview with Abby Smith is a gem.

🔍 Abby's strategy for landing her first junior role:
- Identify a company whose software you've used professionally.
- Explore LinkedIn for similar companies.
- Customize your application materials for each job you find on their websites.
- Cold connect with the team, seeking feedback for continuous improvement.

💪 Juniors, unlock valuable insights! Watch the interview and follow Erik Andersen for expert tips to supercharge your career. Your breakthrough is closer than you think! 🌟💼

Posted on 1/24/2024: https://www.linkedin.com/posts/robertmccleary_programming-jobsearch-softwareengineering-activity-7156084812405968899-gHBe?utm_source=share&utm_medium=member_desktop 

---

Premature optimization is the root of all evil
-- Donald Knuth

Taken From: https://hackernoon.com/40-thought-provoking-software-engineering-quotes-xp2z3tdr

Posted 1/25/2024: https://www.linkedin.com/posts/robertmccleary_premature-optimization-is-the-root-of-all-activity-7156493657083146240-tnLf?utm_source=share&utm_medium=member_desktop 

---

To understand what recursion is, you must first understand recursion.

#fridayfunnies

Taken From: https://www.altcademy.com/blog/the-best-jokes-about-programming-and-software-engineering/ 

posted on 1/26/2022: https://www.linkedin.com/posts/robertmccleary_fridayfunnies-activity-7156848179307569152-O7bx?utm_source=share&utm_medium=member_desktop


