I am grateful for second chances…and thirds and fourths too…

#givethanks

posted on 3/11/2024: https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7173112127845588992-4A9a?utm_source=share&utm_medium=member_desktop

---

Sometimes, the best thing you can do is just say, “I don’t know".

― Titus Winters, Software Engineering at Google: Lessons Learned from Programming Over Time

appropriated from: https://www.goodreads.com/quotes/tag/software-engineering

posted on: 3/14/2024: https://www.linkedin.com/posts/robertmccleary_sometimes-the-best-thing-you-can-do-is-just-activity-7174003187291475969-292d?utm_source=share&utm_medium=member_desktop

---

Why do NFTs make such great presents?

Because they’re GIF certificates.

#fridayfunnies

adapted from: https://discussion.fedoraproject.org/t/my-favorite-tech-joke/46903/30

posted on 3/15/2024: https://www.linkedin.com/posts/robertmccleary_fridayfunnies-activity-7174382211763228672-9Aqh?utm_source=share&utm_medium=member_desktop 

