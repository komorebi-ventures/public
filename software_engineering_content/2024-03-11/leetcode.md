https://leetcode.com/problems/amount-of-time-for-binary-tree-to-be-infected/description/?envType=daily-question&envId=2024-01-10 

For me to undersand how to solve this I needed to learn about binary trees.

Based on what I read a binary tree is a data structure that has a root node and each node can have up to two child nodes.

You can represent a binary tree using an array of node values. 

(30 minutes)

---
Wanted to learn a bit more about binary trees before working on solving this.

The root node is the top most node in the tree. 

You can represent a binary tree as a simple array.
The value in index zero is the root node. Index 1 is the left and index 2 is the right. In the example of the leetcode problem null represents a node that has no value.

You can do simple arithmatic to find a nodes:
Left Child: 2 * index + 1
Right Child: 2 * index + 2
parent: (index - 1) // 2 

I read this to help me. https://www.informit.com/articles/article.aspx?p=3150819&seqNum=11 

Now that I have an understanding of what a binary tree is and and understanding of the problem I need to build a solution. 

Since I will have an array representing the tree and starting value (not a starting index) in the tree that is infected I will need to actually build out the tree to work with. If I had an index for the starting infection location I could ignore building the tree and use a slightly modified array (IE two dimentional) in order to solve the problem using the artymatic option above. As I am writing that idea I think that I can actually find the index of the value given because one of the rules is that each node has a uniqe value and still just use the simple arithmatic to solve the problem. I will try to solve the problem in both ways. I will start with the array version first as I think it will be easier but will also build a full bindary tree just for the experiance of it.

(30 Minutes)

---

It occured to me after rereading the problem and my potential solutions their may be an even simpler arythmatic means of arriving at the answer. knowing the total number of nodes in the tree it seems to me that I should be able to use a simple doubling formula to arrive at an approxamate answer. It would be possible to be wrong on small tree's but if my logic holds there would be a threshold where the total number of minutes it takes for the tree to become completly infected should become the same despite how the infection travels through the network. I will work on that before I move on to another problem as well.

For the first solution I am going to use the value I am given to first find the index of the value in the array. 

void main() {
  int indexOfNodeValue (List root, int start){
      return root.indexOf(start);
    
  }
  
  var value = indexOfNodeValue ([1,2,3], 3);
  print(value);
}

(30 Minutes)

---
