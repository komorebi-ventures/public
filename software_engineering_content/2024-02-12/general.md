I am grateful for mild winter days.

#givethanks

Posted on 2/13/2024 : https://www.linkedin.com/posts/robertmccleary_givethanks-activity-7163352779632177153-C2iQ?utm_source=share&utm_medium=member_desktop 


---

If it hurts, do it more frequently, and bring the pain forward.

-- Jez Humble

taken from: https://www.goodreads.com/quotes/tag/software-engineering 

posted on 2/15/2024: https://www.linkedin.com/posts/robertmccleary_if-it-hurts-do-it-more-frequently-and-bring-activity-7164102099297878016-Ne_e?utm_source=share&utm_medium=member_desktop 

---

I’ve been using Vim for about two years now, mostly because I can’t figure out how to exit.

taken from: https://discussion.fedoraproject.org/t/my-favorite-tech-joke/46903 

posted on 2/16/2024: https://www.linkedin.com/posts/robertmccleary_fridayfunnies-activity-7164439259955810305-yfLQ?utm_source=share&utm_medium=member_desktop 