# Decisions
We use this document to record our decisions as we build Komorebi. The goal here is to help future partners to understand how we choose to do things and the context behind why we made a specific decision. Keeping a clear record should prevent unnecessary rework and provide sufficient details to make changing decisions easier. 

Right now, the only organizational aspect of this document is that we record decisions by date, with the most recent decision on the top.

Be careful to avoid making decisions for problems we aren't facing. For example, it doesn't make sense to add additional organizational rules to this document in its current state because it is so tiny that the simple date structure more than handles our needs. There will undoubtedly be a time when this document will need more structure, but trying to solve it now will be guesswork. In other words, we will have a better idea of the actual organizational problems we need to solve in the future, so holding off on making that decision will lead to better outcomes.

## 3/13/2024
### Getting Started
We have very limited time and energy to devote to our company, yet we need to actually have products to earn an income. Since we only build products we ourselves would use there is a risk that we will build things that are too niche to be viable.  A key strategy we will use to limit this problem is market validation. The tractic we will use to validate our marketability is creating linkedin posts about what we intend to build. At the end of the post we will ask anyone who is interested in the product we are considering building to fill out a google form where we collect email addresses and feedback about features the product should have.  

For each potential product we will estimate the cost to create it and the price we would sell it for. We will guess that 2% of sign ups will actually buy the product when it's ready.  When 2% of total sign ups multiplied by the estimated price equals the amount we estimate the product will cost us we will begin working on the product. 

When we begin working on the product we will also launch a community built on a topic related to the potential product where we can learn from the discussion they have real problems they are hoping to solve that could eventually be incorporated into the product. After the initial version is built we will use what we learn from the community to iterate on our product and the feedback loop here should give us the highest chances at achieving our goal of making a profit.


## 12/13/2023
### Company Focus & Goal
The goal of this company is to create products that we ourselves use. Since we are a small software company than our best market is going to be other small software companies. We intend to build the bussiness so that it comfortably takes care of each crew members financial needs and wants. We will grow slowley but the maximum team size should stay at 9 or less. If people managment ever becomes a full time job we failed. Initially lets stay at 3 or fewer team members.

### Structure of the company
The plan is that everyone who works for us that isn't a contractor will be given ownership and share in the profits shoudl there be any. I am currently thinking we should organize like an old pirate or privateering vessel. 

### New Company Name
We will be changing the name of the company at some point. I don't know what to but Komorebi Ventures is taken in a couple of countries so we will need something else. Maybe somethign creative by mixing up names from old pirate or privateering vessels could be fun. 

## 3/11/2023
### LinkedIn Publishing
Robert has been publishing on LinkedIn on and off for well over a year. It is slowly allowing him to build up a pretty sizable audience to have conversations with, which is excellent, but it is still taking all his development time every week to do. He will start reusing many of his posts, so he should be able to assemble a whole week of material on Saturday. This upcoming week will be the first time he will try that. It will allow him to free up and push forward on building out our forthcoming product.


## 2/15/2023
### Programing Language: Dart  
Why Dart? One of the goals of Komorebi is to build an organization with deep software engineering roots. Google created Dart using what they have learned over many years about software engineering and included things like an established style guide and a native linter. As we progress, we won't need to make these decisions and implement solutions for them. Additionally, because Komorebi will be small, comparatively speaking, we will only be able to have specialized languages for the foreseeable future. Dart provides the ability to engineer the entire stack on all major platforms, which makes it possible for the few of us there will be to collaborate more efficiently on any software project we undertake.  

We considered various other languages as we were making the decision, including TypeScript, C#, and Python.

### Browser: Google Chrome
Google has built powerful development integrations in Dart for chrome that will allow us to debug and experiment much more quickly than we could use another option.

We accept the tradeoff that Google is harvesting our data and could use what they know about us to hinder our company unfairly should they choose to do so.

### IDE: VS Code
We chose Visual Studio Code because Dart (and Flutter) and flutter have official support. It was familiar, cross-platform, lightweight, and free. 

JetBrains also has several IDE's that are supported, but as we are for-profit, the cost is prohibitive, and they are more specialized than we can reasonably account for today.