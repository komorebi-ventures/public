# General Principles
## Be Compassionate (Treat me and the people around me with kindness and understanding.)
- Treat myself and others with kindness and understanding, allowing room for mistakes and recovery.
- Avoid all-or-nothing thinking; return to the routine as soon as possible after deviations.

## Spiritual
### Daily Routine
- **Morning Prayer:**
  - Thank God for another day.
  - Turn my life and will over to Him.
  - Ask for another 24 hours of sobriety.
  - Request an open heart and the Holy Ghost's presence in me.
  - Seek guidance to know and carry out His will.
  - Pray for physical, spiritual, mental, and emotional healing.
  - Ask to be led to the covenant woman I will marry and be sealed to.
  - Pray for my friends and families wellbeing and needs.
- **Scripture Reading:**
  - Read a chapter from the Book of Mormon in the morning.
  - Spend 15 minutes on Come Follow Me study.
  - Read a chapter from the Book of Mormon before bed.
- **Evening Prayer:**
  - Thank God for His help throughout the day.
  - Submit myself to his protection while I sleep.
- Find opportunities to help others.
  - Give cheerful, meaningful service to family and others.
- **Weekly Practices:**
  - Take opportunities on Sundays to study, fast, pray, and draw closer to God.
  - Attend the temple weekly (or monthly).
  - Attend church weekly.

## Mental/Emotional
### I Live in My Beautiful State
- **Core Principles:**
  - Walk moment by moment with the Holy Ghost.
  - Willingly submit to the will of God.
  - When taken out of my beautiful state, take 90 seconds to experience and process emotions, then choose gratitude and joy.
  - Do not make decisions when not in my beautiful state.
  - Recognize life is happening for me, not to me.


### Recovery and Support
- **Daily Actions:**
  - Spend 15 minutes on my current 12-step program step.
  - Reach out for support when fixating on addiction.
  - Complete daily homework from my therapist.
  - Seek daily opportunities to be with real people.
  - Attend all possible 12-step meetings.
  - Surrender lust and character defects to God, asking for His intervention.


### Mindset and Priming
- **Daily Practices:**
  - Do priming every morning.
  - Write in my journal about what I was grateful for during priming.
  - Convert self-talk into prayer.
  - Ensure I am in a peak state before doing anything significant:
    - Jump up and down for 60 seconds.
    - Slow down for 60 seconds.
    - Repeat 7 times.

### Play
- Engage in activities like hiking, spending time at the temple, drone photography, board games, D&D, listening to books, creative writing, studying, scientific research, gardening, learning, dancing, and music (listening, singing, learning guitar). Preferably with family, friends, or others.

## Sleep
- Aim for 8-10 hours of sleep per night.
- Avoid using electronics in bed.
- Maintain a consistent sleep schedule (bed between 8-10 PM, wake at 5:30 AM).
- Prioritize sleep even when interrupted by external factors.

## Eating & Supplementation
### Diet
- **Meal Composition:**
  - 50% green/leafy/cruciferous vegetables.
  - 25% slow-burning carbohydrates (legumes, brown rice, quinoa, sweet potatoes, beets, nuts).
  - 25% lean protein (fish, poultry, eggs, goat, sheep, pork, beef, tofu).
- **Intermittent Fasting:**
  - 20 hours fasting, 4 hours eating (5:30 AM - 9:30 AM).
  - Saturday: 12-hour eating window.
  - Quarterly: 5-day fast.
- **Drinking:**
  - Drink mostly water, some teas (white, green, yerba mate, herbal).
  - Avoid drinking calories.


### Supplements
- **Every Other Day:**
  - NMN & Resveratrol 500 MG each.
  - Quercetin 500 MG.
  - Calcium alpha-ketoglutarate 1000 MG.
  - Spermidine 1.2g.
  - Fisetin 100 MG.
  - Berberine 1g.
  - Alpha-Lipoic Acid 1g.
  - Multi-vitamin without iron.
  - CoQ10 100mg.

## Active Recovery
### Exercise
- Walk 25 miles per week.
- Do calisthenics (pushups, sit-ups, pull-ups) 3 times per week.


### Therapies
- Learn and practice a martial art.
- Engage in yoga.
- Participate in improv comedy or acting classes.
- Nap.
- Practice breath work (Box breathing 4-7-8 for 3 minutes).
- Use cold therapy (cold showers, ice baths).
- Use heat therapy (sauna, hot tub, hot yoga).
- Get massages (massuse, chiropractor, structural integration therapy).
- Meditate.
- Hike in nature.
- Engage in trauma therapy.
- Participate in sexual therapy.
- Attend family therapy sessions.

  

